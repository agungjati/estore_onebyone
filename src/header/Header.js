import React, { Component } from 'react';

class Header extends Component {
    render () {
        return (
            <div>
            <nav className="navbar navbar-fixed-top navbar-light bg-light">
              <div className="container-fluid">
                  <div className="navbar-header">
                      <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                     </button>
                      <a className="navbar-brand" href="#"><img src="/src/logo.png" width="70%"/></a>
                  </div>
          
                  <div id="navbar" className="bg-putih navbar-collapse collapse">
                      <ul className="nav navbar-nav navbar-right">
                          <li className="nav-item">
                              <a className="nav-link" href="#"><img className="icon22 log_alerts" src="/src/log_tasks.png"/><span className="badge badge-warning badge-top">1</span></a>
                          </li>
                          <li className="nav-item">
                              <a className="nav-link" href="#"><img className="icon22 log_alerts" src="/src/log_alerts.png"/><span className="badge badge-warning badge-top">1</span></a>
                          </li>
                          <li className="nav-item dropdown">
                              <a className="nav-link active dropdown-toggle p-a-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false">
                                <img src="/src/people.png" alt="Avatar" className="img-circle" width="40"/> <span name="firstName">George Smith</span>
                            </a>
                              <div className="dropdown-menu dropdown-menu-right dropdown-menu-list" aria-labelledby="Log out">
                                  <button className="dropdown-item" obo-logout="">Logout</button>
                              </div>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav></div>
          );
    }
}

export default Header;