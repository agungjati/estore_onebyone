// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    require('backbone.model.file.upload');
    var commonFunction = require('commonfunction');

    module.exports = Backbone.ModelFileUpload.extend({
        urlRoot: 'Product/UpdateProduct',
        fileAttribute: 'Attachment',
        forceFileMethod: true,
        defaults: function(){
            return {
                ProductName: '',
            }
        },
        initialize: function(options) {
            if (this.beforeInitialize) {
                this.beforeInitialize(options);
            }
            this.on('error', function(model, xhr) {
                require(['commonfunction'], function(commonFunction) {
                    commonFunction.responseStatusNot200({
                        'xhr': xhr
                    });
                });
            });
        }
    });
});
