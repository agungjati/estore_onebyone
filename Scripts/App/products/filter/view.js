// scripts/app/customers/filter
define(function(require, exports, module) {
    'use strict';
    var Filter = require('filter');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Cookies = require('Cookies');
    var commonConfig = require('commonconfig');
    require('select2');

    module.exports = Filter.extend({
        template: _.template(template),
        defaultShowFields: ['ProductName', 'ProductCode', 'CustomerID', 'IsDangerousGoods', 'UnitValue'],
        initialize: function() {
            this.fieldsShowed = this.defaultShowFields.slice();

            commonFunction.setSelect2ProductProductCategory(this, {
                selector: '[name="ProductCategoryName"]',
                placeholder: 'Product Category'
            });

            commonFunction.setSelect2IsDangerousGoods(this);
            commonFunction.setSelect2IsFragileGoods(this);
            commonFunction.setSelect2IsPerishableGoods(this);
            commonFunction.setSelect2IsTemperatureSensitive(this);


            commonFunction.setSelect2CustomerAccountOwner(this, {
                selector: '[name="AccountName"]',
                placeholder: 'Account Owner'
            });

            this.on('beforeRender cleanup', function(){
                if (this.$('.select2-hidden-accessible[name="CustomerID"]').length){
                    this.$('.select2-hidden-accessible[name="CustomerID"]').select2('destroy');
                }
            });

            this.on('afterRender', function() {
                this.$('[name="CustomerName"]').select2({
                    placeholder: 'Customer',
                    minimumInputLength: 1,
                    ajax: {
                        headers: {
                            'Authorization': Cookies.get(commonConfig.cookieFields.Authorization)
                        },
                        url: commonConfig.requestServer + 'Customer',
                        dataType: 'json',
                        type: "GET",
                        quietMillis: 50,
                        data: function(params) {
                            var queryParameters = {
                                DisplayRows: 100,
                                Page: 1,
                                OrderBy: '',
                                FilterParams: [{
                                    Key: 'CustomerName',
                                    Value: params.term
                                }]
                            }
                            return queryParameters;
                        },
                        processResults: function(data) {
                            return {
                                results: $.map(data.ResultList, function(item) {
                                    return {
                                        text: item.CustomerName,
                                        id: item.CustomerID
                                    }
                                })
                            };
                        }
                    }
                });

                this.$('[name="CustomerID"]').on('select2:select', function(){
                });
            });
        }
    });
});
