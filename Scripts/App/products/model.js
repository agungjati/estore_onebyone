define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'ProductID',
        urlRoot: 'Product',
        defaults: function() {
            return {
                ProductName: '',
                ProductCode: '',
                ProductNumber: '',
                ProductDescription: '',
                ProductCategory: {
                    ProductCategoryName: ''
                },
                MadeIn: '',
                Brand: '',
                Customer: {
                    CustomerName: ''
                },
                Supplier: '',
                Barcode: '',
                Manufacture: '',
                ManufactureProductName: '',
                ManufactureProductCode: '',
                ManufactureLeadTime: '',
                SKUType: '',
                StorageChargeUnit: {
                    StorageChargeUnitName: ''
                },
                StorageChargePer: {
                    StorageChargePerName: ''
                },
                TransportChargeUnit: {
                    TransportChargeUnitName: ''
                },
                QuantityPalletPerCarton: '',
                Length: '',
                Width: '',
                Height: '',
                CubicMetres: '',
                WeightKgs: '',
                UnitValue: '',
                ProductColor: '',
                IsDangerousGoods: '',
                IsFragileGoods: '',
                IsPerishableGoods: '',
                IsTemperatureSensitive: '',
                IsActive: '',
                ItemQuantity: '',
                ItemPerInnerQuantity: '',
                ItemPerCartonQuantity: '',
                Weight: '',
                MinimumStockLevel: '',
                ProductPackUnit: {
                    ProductPackUnitName: ''
                },
                UrlDangerousAttach: ''
            }
        }
    });
});
