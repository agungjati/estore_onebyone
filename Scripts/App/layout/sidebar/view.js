define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        el: false,
        initialize: function() {
            this.isLoadingModule = false;
        },
        template: _.template(template),
        events: {
            'click [data-name="menuSidebar"] li a[href]:not([href="#"])': 'clickMenuSidebar'
        },
        clickMenuSidebar: function(e) {
            e.preventDefault();
            var currentTarget = this.$(e.currentTarget);
            var href = currentTarget.attr('href');
            // var getNotHastag = currentTarget.attr('href').substring(1);
            // console.log(currentTarget.find('img').attr('src','/Scripts/img/log_' + getNotHastag +'_click.png'));

            var ret = Backbone.history.navigate(href, true);
            if (ret === undefined) {
                Backbone.history.loadUrl(href);
            }
        },
        loadModuleHome: function() {
            commonFunction.loadModule('./home/homeview')
        }
    });
});
