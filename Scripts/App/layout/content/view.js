define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');

    module.exports = LayoutManager.extend({
        className: 'layout-content',
        attributes: {
            'data-scrollable': ''
        }
    });
});
