// login\app\login
define(function(require, exports, module) {
    'use strict';
    var Backbone = require('backbone');

    module.exports = Backbone.Model.extend({
        idAttribute: 'access_token',
        urlRoot: '../token',
        defaults: {
            username: '',
            password: '',
            grant_type: 'password'
        }
    });
});
