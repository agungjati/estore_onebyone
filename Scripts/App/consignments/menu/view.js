// scripts/app/administration/menu/view
define(function(require, exports, module) {
    'use strict';
    var DefaultModule = require('defaultmodule');
    var template = require('text!./template.html');

    module.exports = DefaultModule.extend({
        template: _.template(template)
    });
});
