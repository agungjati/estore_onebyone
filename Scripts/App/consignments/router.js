//scripts/app/consignments/router
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var Model = require('./model');
    var commonFunction = require('commonfunction');

    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile, replaceMainContent, options) {
        //var hashtag = '#consignments/' + pathViewFile;
        var hashtag = '#consignments/' + ((options && options.appendHashTag) || pathViewFile);

        require(['./' + pathViewFile + '/view'], function(View) {
            var view = new View();

            if (replaceMainContent) {
                commonFunction.setContentViewWithNewModuleView(view, hashtag);
            } else {
                var previousContentView = commonFunction.getContentView();
                var currentContentView = commonFunction.getContentView().getView('');
                var name = previousContentView.getView('') && previousContentView.getView('').name;

                if (name != 'consignments') {
                    var MainView = require('./view');
                    var mainView = new MainView();
                    mainView.setView('[obo-content]', view);
                    commonFunction.setContentViewWithNewModuleView(mainView, hashtag);
                } else {
                    currentContentView.getView('[obo-menu]').doActiveButton();
                    currentContentView.setView('[obo-content]', view);
                    view.render();
                }
            }
        });
    };

    module.exports = Backbone.SubRoute.extend({
        routes: {
            '': 'redirectToConsignments',
            'consignment': 'showConsignment',
            'consignment/add': 'showConsignmentAdd',
            'manifest': 'showManifest',
            'picking': 'showPicking',
            'picking/add': 'showPickingAdd',
            '*subrouter': 'redirectToConsignments'
        },
        showConsignment: function() {
            fnSetContentView('consignment', 'replace');
        },
        showConsignmentAdd: function() {
            fnSetContentView('consignment/add', 'replace', {
                appendHashTag: 'consignment'
            });
        },
        showManifest: function() {
            fnSetContentView('manifest', 'replace');
        },
        showPicking: function() {
            fnSetContentView('picking', 'replace');
        },
        showPickingAdd: function() {
            fnSetContentView('picking/add', 'replace', {
                appendHashTag: 'picking'
            });
        },
        redirectToConsignments: function() {
            this.navigateBackbonePrototype(commonFunction.getCurrentHashToLevel(1).replace('#', '') + '/consignment', {
                trigger: true,
                replace: true
            });
        }
    });
});
