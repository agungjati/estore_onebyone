//scripts/app/products/detail
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var commonFunction = require('commonfunction');
    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile, replaceMainContent) {
        var hashtag = '#orders';
        pathViewFile = pathViewFile || commonFunction.getLastSplitHash();
        require(['./' + pathViewFile + '/view'], function(View) {
            var view = new View();

            if (replaceMainContent) {
                commonFunction.setContentViewWithNewModuleView(view, hashtag);
            } else {

                var previousContentView = commonFunction.getContentView();
                var currentContentView = commonFunction.getContentView().getView('');
                var name = previousContentView.getView('') && previousContentView.getView('').name;

                if (name != 'orders.detail') {
                    var MainView = require('./view');
                    var mainView = new MainView();
                    commonFunction.setContentViewWithNewModuleView(mainView, hashtag);
                    currentContentView = mainView;
                } else {
                    currentContentView.getView('[obo-menu]').doActiveButton();
                }

                currentContentView.setView('[obo-content]', view);
                view.render();
            }
        });
    };

    module.exports = Backbone.SubRoute.extend({
        routes: {
            '': 'redirectToOrderInformation',
            'order_information': 'showModule',
            'pod': 'showModule',
        },
        redirectToOrderInformation: function(){
            this.navigate(this.prefix + '/order_information', {trigger: true});
        },
        showModule: function() {
            fnSetContentView();
        }
    });
});
