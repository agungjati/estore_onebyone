// scripts/app/customers/list
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./row.html');

    module.exports = LayoutManager.extend({
        tagName: 'tr',
        template: _.template(template)
    });
});
