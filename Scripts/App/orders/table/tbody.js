// scripts/app/customers/list
define(function(require, exports, module) {
    'use strict';
    var Marionette = require('marionette');
    var Row = require('./row');

    module.exports = Marionette.CollectionView.extend({
        tagName: 'tbody',
        className: 'hover text-grey',
        childView: Row
    });
});
