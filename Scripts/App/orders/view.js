// scripts/app/customers/
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Table = require('./table/table');
    var Collection = require('./collection');
    require('select2');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.table = new Table({
                collection: new Collection()
            });

            this.on('cleanup', function() {
                this.table.remove();
            });
        },
        afterRender: function() {
            this.$('[obo-table]').append(this.table.el);

            this.table.render();
            this.table.collection.fetch();

            this.$('select').select2();
        },
        events: {
            'click [obo-showeditfilters]': 'showEditFilters',
            'click [obo-clearfilter]': 'clearFilter'
        },
        // showEditFilters: function() {
        //     var self = this;
        //     require(['./modaldialogeditfilter/view'], function(ModalDialog) {
        //         var modalDialog = new ModalDialog();
        //         $('body').append(modalDialog.el);
        //         modalDialog.$el.on('hidden.bs.modal', function() {
        //             modalDialog.remove();
        //         });
        //
        //         modalDialog.once('afterRender', function() {
        //             modalDialog.$el.modal();
        //         });
        //         modalDialog.render();
        //
        //
        //     });
        // },
        clearFilter: function(e) {
            console.log('clear filter');
        }
    });
});
