// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');

    module.exports = Model.extend({
        urlRoot: 'customer/createcustomer',
        defaults: function(){
            return {
                AccountNo: '',
                CustomerName: '',
                CustomerNo: '',
                CustoemrTradingAs: '',
                CustomerABN: '',
                CustomerACN: '',
                DateStart: '',
                AccountOwnerID: '',
                PaymentTermID: '1010',
                Phone: '',
                Fax: '',
                Mobile: '',
                InvoiceEmail: '',
                PodEmail: '',
                FreightPayableBy: '',
                CreditLimit: '',
                InsertDate: ''
            }
        }
    });
});
