define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'StorageLocationID',
        urlRoot: 'storage/storageLocation',
        defaults: function() {
            return {
                LocationCode: '',
                Building:'',
                Isle:'',
                Bay:'',
                MaxHeight:'',
                MaxLength:'',
                MaxQuantity:'',
                MaxWeightKgs:'',
                MaxWidth:'',
                StorageLocationID:'',
                Tier:'',
                ZoneType: {
                    ZoneTypeName:''
                },
                ZoneArea: {
                    ZoneAreaName:''
                },
                SKUType: {
                    SKUTypeName:''
                },
                IsActive: '',
                LocationScetchOriginalFileName:'',
                LocationScetchFileName:'',
                LocationScetchURL:'',
            }
        }
    });
});
