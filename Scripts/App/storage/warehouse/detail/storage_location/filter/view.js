// scripts/app/customers/filter
define(function(require, exports, module) {
    'use strict';
    var Filter = require('filter');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    require('datetimepicker');

    module.exports = Filter.extend({
        className:'',
        template: _.template(template),
        defaultShowFields: ['LocationCode', 'ZoneType', 'ZoneArea', 'SKUType', 'IsActive'],
        initialize: function() {
            this.fieldsShowed = this.defaultShowFields.slice();
            commonFunction.setSelect2StatusStorageLocation(this,{
                selector : '[name="IsActive"]',
                placeholder: 'Status'
            });
            commonFunction.setSelect2ProductSKUType(this,{
                selector : '[name="SKUType"]',
                placeholder: 'SKU Type'
            });
            commonFunction.setSelect2ZoneType(this,{
                selector: '[name="ZoneType"]',
                placeholder: 'Zone Type'
            });
            commonFunction.setSelect2ZoneArea(this,{
                selector: '[name="ZoneArea"]',
                placeholder: 'Zone Area'
            });
        }
    });
});
