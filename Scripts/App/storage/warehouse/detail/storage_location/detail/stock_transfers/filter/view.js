// scripts/app/customers/detail/senders_and_recievers/filter
define(function(require, exports, module) {
    'use strict';
    var Filter = require('filter');
    var template = require('text!./template.html');
    var Cookies = require('Cookies');
    var commonConfig = require('commonconfig');
    var commonFunction = require('commonfunction');
    require('datetimepicker');
    require('select2');

    module.exports = Filter.extend({
        tagName: 'form',
        className: 'row',
        template: _.template(template),
        defaultShowFields: ['insertdate', 'productname', 'sourcelocationcode', 'destinationlocationcode', 'transferqty'],
        initialize: function() {
            this.fieldsShowed = this.defaultShowFields.slice();
            this.on('afterRender', function() {
                this.$('[name="productname"]').select2({
                    placeholder: 'Product Name',
                    minimumInputLength: 1,
                    ajax: {
                        headers: {
                            'Authorization': Cookies.get(commonConfig.cookieFields.Authorization)
                        },
                        url: commonConfig.requestServer + 'Product',
                        dataType: 'json',
                        type: "GET",
                        quietMillis: 50,
                        data: function(params) {
                            var queryParameters = {
                                DisplayRows: 100,
                                Page: 1,
                                OrderBy: '',
                                FilterParams: [{
                                    Key: 'ProductName',
                                    Value: params.term
                                }]
                            }
                            return queryParameters;
                        },
                        processResults: function(data) {
                            return {
                                results: $.map(data.ResultList, function(item) {
                                    return {
                                        text: item.ProductName,
                                        id: item.ProductID
                                    }
                                })
                            };
                        }
                    }
                });

                this.$('[name="sourcelocationcode"]').select2({
                    placeholder: 'Source',
                    //minimumInputLength: 1,
                    ajax: {
                        headers: {
                            'Authorization': Cookies.get(commonConfig.cookieFields.Authorization)
                        },
                        url: commonConfig.requestServer + 'Storage/'+ commonFunction.getUrlHashSplit(4) +'/StorageLocation',
                        dataType: 'json',
                        type: "GET",
                        quietMillis: 50,
                        data: function(params) {
                            var queryParameters = {
                                DisplayRows: 100,
                                Page: 1,
                                OrderBy: '',
                                FilterParams: [{
                                    Key: 'LocationCode',
                                    Value: (( params.term == undefined ) ? '' : params.term )
                                }]
                            }
                            return queryParameters;
                        },
                        processResults: function(data) {
                            return {
                                results: $.map(data.ResultList, function(item) {
                                    return {
                                        text: item.LocationCode,
                                        id: item.StorageLocationID
                                    }
                                })
                            };
                        }
                    }
                });

                this.$('[name="destinationlocationcode"]').select2({
                    placeholder: 'Destination',
                    //minimumInputLength: 1,
                    ajax: {
                        headers: {
                            'Authorization': Cookies.get(commonConfig.cookieFields.Authorization)
                        },
                        url: commonConfig.requestServer + 'Storage/'+ commonFunction.getUrlHashSplit(4) +'/StorageLocation',
                        dataType: 'json',
                        type: "GET",
                        quietMillis: 50,
                        data: function(params) {
                            var queryParameters = {
                                DisplayRows: 100,
                                Page: 1,
                                OrderBy: '',
                                FilterParams: [{
                                    Key: 'LocationCode',
                                    Value: (( params.term == undefined ) ? '' : params.term )
                                }]
                            }
                            return queryParameters;
                        },
                        processResults: function(data) {
                            return {
                                results: $.map(data.ResultList, function(item) {
                                    return {
                                        text: item.LocationCode,
                                        id: item.StorageLocationID
                                    }
                                })
                            };
                        }
                    }
                });

                this.$('[name="sourcestockavailable"]').select2();
                this.$('[name="destinationstockavailable"]').select2();
            });
            // commonFunction.setSelect2ContactType(this,{
            //     selector:'[name="ContactTypeName"]',
            //     placeholder:'Contact Type'
            // });
            //
            // commonFunction.setSelect2Status(this, {
            //     selector: '[name="IsActive"]',
            //     placeholder: 'Status'
            // });
            this.on('cleanup', function(){
                this.$('[name="sourcestockavailable"]').select2('destroy');
                this.$('[name="destinationstockavailable"]').select2('destroy');
            })
        },
        afterShowHideFieldsFilter: function() {
            var self = this;
            if (this.$('[name="insertdate"]:visible').length) {
                this.$('[name="insertdate"]').datetimepicker({
                    format: commonConfig.datePickerFormat
                }).on('dp.change', function(event){
                    self.doFilterAndSorting(event);
                });

            }else{
                if (this.$('[name="insertdate"]').data('DateTimePicker') && this.$('[name="insertdate"]').data('DateTimePicker').length){
                    this.$('[name="insertdate"]').data('DateTimePicker').destroy();
                }
            }
        },
    });
});
