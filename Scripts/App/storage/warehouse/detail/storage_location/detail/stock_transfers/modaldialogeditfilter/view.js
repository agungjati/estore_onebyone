// scripts/app/customers/detail/contacts/modaldialogeditfilter
define(function(require, exports, module) {
    'use strict';
    var View = require('modaldialogeditfilter');
    var template = require('text!./template.html');

    module.exports = View.extend({
        template: _.template(template),
    });
});
