//scripts/app/storage/warehouse/detail/storage_location/detail
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var Model = require('./model');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');

    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile, replaceMainContent, options) {
        var hashtag = '#storage/warehouse';

        eventAggregator.trigger('getStorageWarehouseStorageLocationModel');
        require(['./' + pathViewFile + '/view'], function(View) {
            var view = new View(options);

            if (replaceMainContent) {
                commonFunction.setContentViewWithNewModuleView(view, hashtag);
            } else {
                var previousContentView = commonFunction.getContentView();
                var currentContentView = commonFunction.getContentView().getView('');
                var name = previousContentView.getView('') && previousContentView.getView('').name;

                if (name != 'storage.warehouse.detail.storage_location.detail') {
                    var MainView = require('./view');
                    var mainView = new MainView();
                    mainView.setView('[obo-content]', view);
                    commonFunction.setContentViewWithNewModuleView(mainView, hashtag);
                } else {
                    currentContentView.getView('[obo-menu]').doActiveButton();
                    currentContentView.setView('[obo-content]', view);
                    view.render();
                }
            }
        });
    };

    module.exports = Backbone.SubRoute.extend({
        initialize: function() {
            this.app = {};
            this.model = new Model();

            this.stopListening(eventAggregator, 'getStorageWarehouseStorageLocationModel');
            this.listenTo(eventAggregator, 'getStorageWarehouseStorageLocationModel', function(callback){
                var warehouseID = commonFunction.getUrlHashSplit(4);
                var storageLocationID = commonFunction.getUrlHashSplit(7);
                var url = 'storage/' + warehouseID + '/storageLocation/';
                if (this.model.urlRoot != url || (String(this.model.id || '') != storageLocationID)){
                    this.model.urlRoot = url;
                    this.model.clear({silent: true}).set(this.model.defaults());
                    this.model.set(this.model.idAttribute, storageLocationID);
                    this.model.once('sync', function(model){
                        callback && callback(model);
                    });
                    if (!this.model.requestToServer){
                        this.model.fetch();
                    }

                }else if (!this.model.requestToServer){
                    callback && callback(this.model);
                }else{
                    this.model.once('sync', function(model){
                        callback && callback(model);
                    });
                }
            });
        },
        routes: {
            'storage_location_information': 'showStorageLocationInformation',
            'storage_location_information/edit': 'showStorageLocationInformationEdit',
            'storage_balance': 'showStorageBalance',
            'storage_history': 'showStorageHistory',
            'storage_transfers': 'showStockTransfer',
            'storage_transfers/add': 'showStockTransferAdd',
            'storage_transfers/detail/:id': 'showStockTransferView',
            'storage_transfers/detail/:id/edit': 'showStockTransferEdit',
            '*subrouter': 'redirectToShowStorageLocationInformation',
        },
        showStorageLocationInformation: function(){
            debugger
            fnSetContentView('storage_location_information', '', {
                model: this.model
            });
        },
        showStorageLocationInformationEdit: function(){
            fnSetContentView('storage_location_information/edit', 'replace');
        },
        showStorageBalance : function(){
            fnSetContentView('storage_balance');
        },
        showStorageHistory : function(){
            fnSetContentView('storage_history');
        },
        showStockTransfer : function(){
            fnSetContentView('stock_transfers');
        },
        showStockTransferAdd : function(){
            fnSetContentView('stock_transfers/add', 'replace', {
                model: this.model
            });
        },
        showStockTransferView : function(){
            fnSetContentView('stock_transfers/view', 'replace');
        },
        showStockTransferEdit : function(){
            fnSetContentView('stock_transfers/view/edit', 'replace');
        },
        // redirectToShowStorageLocationInformation: function(){
        //     this.navigateBackbonePrototype(commonFunction.getCurrentHashToLevel(7).replace('#', '') + '/storage_location_information', {
        //         trigger: true,
        //         replace: true
        //     });
        // }
    });
});
