// scripts\app\storage\warehouse\detail\storage_location\detail\menu
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Menu = require('menu');

    module.exports = Menu.extend({
        template: _.template(template)
    });
});
