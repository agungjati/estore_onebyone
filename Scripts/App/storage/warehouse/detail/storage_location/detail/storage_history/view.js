// scripts/app/customers/detail/addresses
define(function(require, exports, module) {
    'use strict';
    var View = require('./../../../../commons/view');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Paging = require('paging');
    var Filter = require('./filter/view');

    module.exports = View.extend({
        template: _.template(template),
        initialize: function() {
            this.table = new Table({
                collection: new Collection()
            });

            // this.table.collection.url = 'Customer/' + commonFunction.getUrlHashSplit(3) + '/Address';
            // this.table.collection.url = 'Storage/Warehouse/'+ commonFunction.getUrlHashSplit(4) +'/Location/'+ commonFunction.getUrlHashSplit(7) +'/StorageHistory.json';

            this.filter = new Filter({
                collection: this.table.collection
            });

            this.paging = new Paging({
                collection: this.table.collection
            })

            //   commonFunction.setSelect2AddressType(this);

            this.on('cleanup', function() {
                this.table.destroy();
                this.modalDialog && this.modalDialog.remove && this.modalDialog.remove();
            }, this)
        },
        afterRender: function() {
            this.insertView('[obo-filter]', this.filter);
            this.filter.render();

            this.$('[obo-table]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.table.collection.fetch();
        },
        // setFilterToDefault: function() {
        //     this.filter.setFilterToDefault();
        // },
        showEditFilters: function() {
            var self = this;
            require(['./modaldialogeditfilter/view'], function(ModalDialog) {
                self.modalDialog = new ModalDialog({
                    viewFilter: self.filter
                });

                $('body').append(self.modalDialog.el);
                self.modalDialog.$el.on('hidden.bs.modal', function() {
                    self.modalDialog.remove();
                });

                self.modalDialog.once('afterRender', function() {
                    self.modalDialog.$el.modal();
                });
                self.modalDialog.render();
            });
        }
    });
});
