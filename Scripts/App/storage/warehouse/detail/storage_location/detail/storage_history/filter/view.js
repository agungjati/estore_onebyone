// scripts/app/customers/filter
define(function(require, exports, module) {
    'use strict';
    var Filter = require('filter');
    var template = require('text!./template.html');
    var Cookies = require('Cookies');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    require('datetimepicker');

    module.exports = Filter.extend({
        template: _.template(template),
        className : '',
        defaultShowFields: ['insertdate', 'productname', 'StorageHistory', 'AlteredDescription', 'Quantity'],
        initialize: function() {
            this.fieldsShowed = this.defaultShowFields.slice();
            this.on('afterRender', function() {
                this.$('[name="productname"]').select2({
                    minimumInputLength: 1,
                    ajax: {
                        headers: {
                            'Authorization': Cookies.get(commonConfig.cookieFields.Authorization)
                        },
                        url: commonConfig.requestServer + 'Product',
                        dataType: 'json',
                        type: "GET",
                        quietMillis: 50,
                        data: function(params) {
                            var queryParameters = {
                                DisplayRows: 100,
                                Page: 1,
                                OrderBy: '',
                                FilterParams: [{
                                    Key: 'ProductName',
                                    Value: params.term
                                }]
                            }
                            return queryParameters;
                        },
                        processResults: function(data) {
                            return {
                                results: $.map(data.ResultList, function(item) {
                                    return {
                                        text: item.ProductName,
                                        id: item.ProductID
                                    }
                                })
                            };
                        }
                    }
                });
            })
            // commonFunction.setSelect2CustomerAccountOwner(this, {
            //     selector: '[name="AccountName"]',
            //     placeholder: 'Account Owner'
            // });
            //
            // commonFunction.setSelect2CustomerTags(this, {
            //     selector: '[name="TagName"]',
            //     placeholder: 'Tag'
            // });
            //
            // commonFunction.setSelect2Status(this, {
            //     selector: '[name="IsActive"]',
            //     placeholder: 'Status'
            // });

            this.on('cleanup', function(){
                if (this.$('[name="DateStart"]').data('DateTimePicker') && this.$('[name="DateStart"]').data('DateTimePicker').length){
                    this.$('[name="DateStart"]').data('DateTimePicker').destroy();
                }
            });
        },
        afterShowHideFieldsFilter: function() {
            var self = this;
            if (this.$('[name="insertdate"]:visible').length) {
                this.$('[name="insertdate"]').datetimepicker({
                    format: commonConfig.datePickerFormat
                }).on('dp.change', function(event){
                    self.doFilterAndSorting(event);
                });

            }else{
                if (this.$('[name="insertdate"]').data('DateTimePicker') && this.$('[name="insertdate"]').data('DateTimePicker').length){
                    this.$('[name="insertdate"]').data('DateTimePicker').destroy();
                }
            }
        }
    });
});
