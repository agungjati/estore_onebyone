// scripts/app/storage/warehouse/detail/warehouse_profile/edit
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var Model = require('./../../../model');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    var ladda = require('ladda.jquery');
    require('maxlength');
    require('select2');
    require('jquery-ui');
    require('bootstrap-validator');

    module.exports = LayoutManager.extend({
        className: 'container-fluid',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();

            this.model.set(this.model.idAttribute, commonFunction.getUrlHashSplit(4));

            this.listenTo(this.model, 'request', function() {
                self.$('[obo-dosave]').attr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('start');
            });

            this.listenTo(this.model, 'sync', function() {
                eventAggregator.trigger('getStorageWarehouseModel', function(model){
                    model.set(self.model.toJSON());
                })
            });

            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.listenToOnce(this.model, 'sync', function(model) {
                this.render();
                var data = model.toJSON();
                commonFunction.setSelect2CustomerAccountOwner(this, {
                    selector: '[name="WarehouseOwnerID"]'
                }).then(function() {
                    self.$('[name="WarehouseOwnerID"]').val(data.WarehouseOwner.AccountOwnerID).trigger("change");
                });

                commonFunction.setSelect2GeneralCountry(this, {
                    selector: '[name="CountryID"]'
                }).then(function() {
                    self.$('[name="CountryID"]').val(data.Country.CountryID).trigger('change');
                });

                commonFunction.setSelect2Status(this, {
                    selector: '[name="IsActive"]'
                }).then(function() {
                    var found = data.IsActive;
                    self.$('[name="IsActive"]').val(found.toString()).trigger('change');
                });

                commonFunction.setSelect2WarehouseManagementType(this).then(function() {
                    self.$('[name="WarehouseManagementTypeID"]').val(data.WarehouseManagementType.WarehouseManagementTypeID).trigger('change');
                });

                commonFunction.setSelect2WarehouseManager(this).then(function() {
                    self.$('[name="WarehouseManagerID"]').val(data.WarehouseManager.WarehouseManagerID).trigger('change');
                });

                this.listenTo(this.model, 'sync', function() {
                    Backbone.history.navigate(commonFunction.getCurrentHashOmitSuffix(1), true);
                });

            }, this);

            this.once('afterRender', function() {
                this.model.fetch();
            });

            this.on('cleanup', function() {
                this.laddaDestroy();
            });
        },
        events: {
            'focus [name="Address1"]': 'getAutocompleteGooglePlace1',
            'focusout [name="Address1"]': 'checkAutoComplete1',
            'keydown [name="Address1"]': 'removeWarningMessage1'
        },
        beforeRender: function() {
            this.laddaDestroy();
        },
        afterRender: function() {
            if (!this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            }

            this.$('[name="WarehouseDescription"]').maxlength({
                max: 500,
                feedbackText: 'Text limited to {m} characters. Remaining: {r}'
            });

            this.renderValidation();
        },
        renderValidation: function() {
            var self = this;
            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        Phone: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Phone Number is invalid'
                                }
                            }
                        },
                        Fax: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Fax Number is invalid'
                                }
                            }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        removeWarningMessage1: function() {
            this.$('[obo-warningMessage1]').remove();
        },
        checkAutoComplete1: function() {
            var self = this;
            var dom = this.$('[name="Address1"]');
            this.removeWarningMessage1();

            var found = _.find($('.ui-autocomplete li'), function(item) {
                return dom.val() == item.textContent;
            });

            if (!found) {
                dom.val('');
                self.$('[obo-dosave]').prop("disabled", true);
                var html = $('<small class="text-warning" style="display: inline;" obo-warningMessage1>Address Not Found</small>')
                self.$('[obo-view="error1"]').append(html);
                this.setAddresses();
            } else {
                self.removeWarningMessage1();
                self.$('[obo-dosave]').prop("disabled", false);
            }
        },
        getAutocompleteGooglePlace1: function() {
            var self = this;

            this.$('[name="Address1"]').autocomplete({
                source: function(req, resp) {
                    commonFunction.getGoogleMapAutoCompleteService().getPlacePredictions({
                        input: req.term,
                        types: ['geocode'],
                        componentRestrictions: {
                            country: 'au'
                        }
                    }, function(places, status) {
                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                            var _places = [];
                            for (var i = 0; i < places.length; ++i) {
                                _places.push({
                                    id: places[i].place_id,
                                    value: places[i].structured_formatting.main_text,
                                    label: places[i].structured_formatting.main_text
                                });
                            }
                            self.$('[obo-dosave]').prop("disabled", false);
                            resp(_places);
                        } else {
                            $('.ui-autocomplete').css('display', 'none');
                            var html = $('<small class="text-warning" style="display: inline;" obo-warningMessage1>Address Not Found</small>')
                            self.$('[obo-view="error1"]').append(html);
                            self.setAddresses();
                            self.$('[obo-dosave]').prop("disabled", true);
                        }
                    });
                },
                select: function(event, object) {
                    if (object && object.item && object.item.id) {
                        commonFunction.getGoogleMapPlacesPlaceService().getDetails({
                            placeId: object.item.id
                        }, function(places, status) {
                            self.setAddresses(commonFunction.getGoogleMapDetailResult(places, status));
                        });
                    } else {
                        self.setAddresses();
                    }
                }
            });
        },
        setAddresses: function(results) {
            this.$('input[name="PostCode"]').val((results && results.postcode) || '');
            this.$('input[name="City"]').val((results && results.city) || '');
            this.$('input[name="State"]').val((results && results.state) || '');

            this.$('[name="Country"] option').filter(function() {
                return $(this).text() == (results && results.country);
            }).prop('selected', true).trigger('change');
        },
        doSave: function(e) {
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            data.Country = this.$('[name="CountryID"]').select2('data')[0].text;

            data.PostCode = this.$('[name="PostCode"]').val();
            data.State = this.$('[name="State"]').val();
            data.City = this.$('[name="City"]').val();

            this.model.save(data);
        },
        laddaDestroy: function() {
            if (this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'].remove();
                delete this.ladda['obo-dosave'];
            }
        }
    });
});
