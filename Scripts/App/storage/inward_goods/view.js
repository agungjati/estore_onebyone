// scripts/app/storage/warehouse
define(function(require, exports, module) {
    'use strict';
    var DefaultModule = require('defaultmodule');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Filter = require('./filter/view');
    var ModalDialog = require('./modaldialogeditfilter/view');
    var Paging = require('paging');
    // require('select2');

    module.exports = DefaultModule.extend({
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.table = new Table({
                collection: new Collection()
            });

            this.filter = new Filter({
                collection: this.table.collection
            });

            this.paging = new Paging({
                collection: this.table.collection
            });

            this.ModalDialog = ModalDialog;

            // this.collectionAccountOwner = commonFunction.getCollectionCustomerAccountOwner();
            //
            // this.collectionAccountOwner.once('sync', function(collection) {
            //     self.$('select[name="accountowner"]').empty();
            //     self.$('select[name="accountowner"]').append('<option selected disabled>Account Owner</option>');
            //     collection && _.each(collection.models, function(model) {
            //         self.$('select[name="accountowner"]').append(new Option(model.get('AccountName'), model.get('AccountNo')));
            //     })
            // });

            this.on('cleanup', function() {
                this.table.destroy();
            });
        },
        afterRender: function() {
            this.insertView('[obo-filter]', this.filter);
            this.filter.render();

            this.$('[obo-table]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.table.collection.fetch();

            // if (!this.collectionAccountOwner.length) {
            //     this.collectionAccountOwner.fetch();
            // } else {
            //     this.collectionAccountOwner.trigger('sync');
            // }
            // this.$('select').select2();
            // this.$('[obo-table]').append(this.table.el);
            //
            // this.table.collection.reset([{
            //     id: 1,
            //     WarehouseType: 'AC2',
            //     WarehouseName: 'ACC100223334',
            //     Address: 'B8/63 - 69 Pipe Road',
            //     Country: 'Australia',
            //     WarehouseManager: 'Evie S.Gowing'
            // }, {
            //     id: 2,
            //     WarehouseType: 'GLC',
            //     WarehouseName: 'GLB236598661',
            //     Address: 'G1, 11 - 21 Forge Street',
            //     Country: 'Australia',
            //     WarehouseManager: 'Paige Labilliere'
            // }, {
            //     id: 3,
            //     WarehouseType: '3WO',
            //     WarehouseName: 'WO866952333',
            //     Address: '23 Walters Street',
            //     Country: 'Australia',
            //     WarehouseManager: 'Sam Hearn'
            // }, {
            //     id: 4,
            //     WarehouseType: 'HXX',
            //     WarehouseName: 'HOL777555666',
            //     Address: '1628 Ipswich Road',
            //     Country: 'Australia',
            //     WarehouseManager: 'Active'
            // }, {
            //     id: 5,
            //     WarehouseType: 'I09T',
            //     WarehouseName: 'INT889933222',
            //     Address: '126 Churchill Road North',
            //     Country: 'Australia',
            //     WarehouseManager: 'Active'
            // }]);
            // this.table.render();
            //this.tableBody.collection.fetch();
        },
    });
});
