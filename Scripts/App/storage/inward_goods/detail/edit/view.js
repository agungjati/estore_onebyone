// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    // var Model = require('./../../model');
    var ModelPut = require('./model');
    var ladda = require('ladda.jquery');
    var eventAggregator = require('eventaggregator');
    require('select2');
    require('maxlength');
    require('jquery-ui');
    require('datetimepicker');
    require('bootstrap-validator');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            // this.model = new Model();
            this.modelPut = new ModelPut();

            // this.model.set(this.model.idAttribute, commonFunction.getUrlHashSplit(3));
            // this.listenToOnce(this.model, 'sync', function(model) {
            //     this.once('afterRender', function() {
            //
            //         // if (data && data.Customer && data.Customer.CustomerName) {
            //         //     this.$('[name="CustomerID"]').append(new Option(data.Customer.CustomerName, data.Customer.CustomerID)).trigger('change');
            //         // }
            //
            //     });
            //
            //     this.render();
            //     var data = model.toJSON();
            //     this.listenTo(this.model, 'sync', function() {
            //         Backbone.history.navigate('products', true);
            //     });
            //
            // }, this);
            
            this.once('afterRender', function() {
                // this.model.fetch();
            });

            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled').html('SAVE');
                // self.ladda['obo-dosave'].ladda('stop');
            });
        },
        events: {
            // 'click [obo-dosave]': 'doSave',
            'focus [name="Address1"]': 'getAutocompleteGooglePlace1',
            'keydown [name="Address1"]': 'removeWarningMessage1',
            'focusout [name="Address1"]': 'checkAutoComplete1'
        },
        beforeRender: function() {
            this.laddaDestroy();
        },
        afterRender: function() {
            this.$('[name="ReceivedDate"]').datetimepicker({
                defaultDate: new Date(),
                format: commonConfig.datePickerFormat
            });
            if (!this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            }
            this.$('[name="ProductCode"]').maxlength({
                max: 500,
                feedbackText: 'Text limited to {m} characters. Remaining: {r}'
            });
            this.renderValidation();
        },
        removeWarningMessage1: function() {
            this.$('[obo-warningMessage1]').remove();
        },
        checkAutoComplete1: function(e) {
            var self = this;
            var dom = this.$('[name="Address1"]');
            this.removeWarningMessage1();

            var found = _.find($('.ui-autocomplete li'), function(item) {
                return dom.val() == item.textContent;
            });

            if (!found) {
                dom.val('');
                this.$('[obo-dosave]').prop("disabled", true);
                var html = $('<small class="text-danger" style="display: inline;" obo-warningMessage1>Address Not Found</small>')
                this.$('[obo-view="error1"]').append(html);
                this.setAddresses();
            } else {
                this.removeWarningMessage1();
                this.$('[obo-dosave]').prop("disabled", false);
            }
        },
        getAutocompleteGooglePlace1: function() {
            var self = this;

            this.$('[name="Address1"]').autocomplete({
                source: function(req, resp) {
                    commonFunction.getGoogleMapAutoCompleteService().getPlacePredictions({
                        input: req.term,
                        types: ['geocode'],
                        componentRestrictions: {
                            country: 'au'
                        }
                    }, function(places, status) {
                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                            var _places = [];
                            for (var i = 0; i < places.length; ++i) {
                                _places.push({
                                    id: places[i].place_id,
                                    value: places[i].structured_formatting.main_text,
                                    label: places[i].structured_formatting.main_text
                                });
                            }
                            self.$('[obo-dosave]').prop("disabled", false);
                            resp(_places);
                        } else {
                            $('.ui-autocomplete').css('display', 'none');
                            var html = $('<small class="text-danger" style="display: inline;" obo-warningMessage1>Address Not Found</small>')
                            self.$('[obo-view="error1"]').append(html);
                            self.setAddresses();
                            self.$('[obo-dosave]').prop("disabled", true);
                        }
                    });
                },
                select: function(event, object) {
                    if (object && object.item && object.item.id) {
                        commonFunction.getGoogleMapPlacesPlaceService().getDetails({
                            placeId: object.item.id
                        }, function(places, status) {
                            self.setAddresses(commonFunction.getGoogleMapDetailResult(places, status));

                        });
                    } else {
                        self.setAddresses();
                    }
                }
            });
        },
        setAddresses: function(results) {
            this.$('input[name="Postcode"]').val((results && results.postcode) || '');
            this.$('input[name="State"]').val((results && results.state) || '');
            this.$('input[name="City"]').val((results && results.city) || '');

            this.$('[name="CountryID"] option').filter(function() {
                return $(this).text() == (results && results.country);
            }).prop('selected', true).trigger('change');

        },
        renderValidation: function() {
            var self = this;
            this.$('[obo-form]').bootstrapValidator({
                    // fields: {
                    //     Length:{
                    //         validators: {
                    //             notEmpty: {
                    //             },
                    //             numeric: {
                    //                 message: 'The Length must be a numeric number'
                    //             }
                    //         }
                    //     },
                    //     Width:{
                    //         validators: {
                    //             notEmpty: {
                    //             },
                    //             numeric: {
                    //                 message: 'The Width must be a numeric number'
                    //             }
                    //         }
                    //     },
                    //     Height:{
                    //         validators: {
                    //             notEmpty: {
                    //             },
                    //             numeric: {
                    //                 message: 'The Width must be a numeric number'
                    //             }
                    //         }
                    //     },
                    //     Weight:{
                    //         validators: {
                    //             notEmpty: {
                    //             },
                    //             numeric: {
                    //                 message: 'The Weight must be a numeric number'
                    //             }
                    //         }
                    //     },
                    // }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        doSave: function(e) {
            var self = this;
            var data = {};
            if (e && e.currentTarget) {
                $(e.currentTarget).attr('disabled', 'disabled').html('Loading...');
            }
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            data.id = commonFunction.getUrlHashSplit(3);
        },
        laddaDestroy: function() {
            if (this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'].remove();
                delete this.ladda['obo-dosave'];
            }
        }
    });
});
