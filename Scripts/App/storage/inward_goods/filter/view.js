// scripts/app/customers/filter
define(function(require, exports, module) {
    'use strict';
    var Filter = require('filter');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    require('datetimepicker');

    module.exports = Filter.extend({
        template: _.template(template),
        defaultShowFields: ['ReceiveDate', 'Reason', 'ProductName', 'UnitValue', 'GoodCondition'],
        initialize: function() {
            this.fieldsShowed = this.defaultShowFields.slice();

            // commonFunction.setSelect2CustomerAccountOwner(this, {
            //     selector: '[name="AccountName"]',
            //     placeholder: 'Account Owner'
            // });

            this.on('cleanup', function(){

            });
        },
        afterShowHideFieldsFilter: function() {
            var self = this;
        }
    });
});
