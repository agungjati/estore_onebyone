// scripts/app/storage/warehouse/detail
define(function(require, exports, module) {
    'use strict';
    var Menu = require('menu');
    var template = require('text!./template.html');

    module.exports = Menu.extend({
        template: _.template(template)
    });
});
