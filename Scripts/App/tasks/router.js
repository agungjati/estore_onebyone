//scripts/app/storage/warehouse/detail
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var Model = require('./model');
    var commonFunction = require('commonfunction');

    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile, replaceMainContent) {
        var hashtag = '#tasks';

        require(['./' + pathViewFile + '/view'], function(View) {
            var view = new View();

            if (replaceMainContent) {
                commonFunction.setContentViewWithNewModuleView(view, hashtag);
            } else {

                var previousContentView = commonFunction.getContentView();
                var currentContentView = commonFunction.getContentView().getView('');
                var name = previousContentView.getView('') && previousContentView.getView('').name;

                if (name != 'tasks') {
                    var MainView = require('./view');
                    var mainView = new MainView();
                    mainView.setView('[obo-content]', view);
                    commonFunction.setContentViewWithNewModuleView(mainView, hashtag);
                } else {
                    currentContentView.getView('[obo-menu]').doActiveButton();
                    currentContentView.setView('[obo-content]', view);
                    view.render();
                }
            }
        });
    };

    module.exports = Backbone.SubRoute.extend({
        routes: {
            '': 'redirectToShowMyTasks',
            'my_tasks': 'showMyTasks',
            'my_tasks/add': 'showMyTasksAdd',
            'all_tasks': 'showAllTasks',
            'all_tasks/add': 'showAllTasksAdd',
            '*subrouter': 'redirectToShowMyTasks'
        },
        showMyTasks: function() {
            fnSetContentView('my_tasks');
        },
        showMyTasksAdd: function() {
            fnSetContentView('my_tasks/add', 'replace');
        },
        showAllTasks: function() {
            fnSetContentView('all_tasks');
        },
        showAllTasksAdd: function() {
            fnSetContentView('all_tasks/add', 'replace');
        },
        redirectToShowMyTasks: function() {
            this.navigateBackbonePrototype(commonFunction.getCurrentHashToLevel(1).replace('#', '') + '/my_tasks', {
                trigger: true,
                replace: true
            });
        }
    });
});
