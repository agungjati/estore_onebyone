//scripts/app/customers/detail
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var Model = require('./model');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');


    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile, replaceMainContent) {
        var hashtag = '#customers';

        require(['./' + pathViewFile + '/view'], function(View) {
            var view = new View();

            if (replaceMainContent) {
                commonFunction.setContentViewWithNewModuleView(view, hashtag);
            } else {

                var previousContentView = commonFunction.getContentView();
                var currentContentView = commonFunction.getContentView().getView('');
                var name = previousContentView.getView('') && previousContentView.getView('').name;

                if (name != 'customers.detail') {
                    var MainView = require('./view');
                    var mainView = new MainView();
                    commonFunction.setContentViewWithNewModuleView(mainView, hashtag);
                    currentContentView = mainView;
                } else {
                    currentContentView.getView('[obo-menu]').doActiveButton();
                }

                currentContentView.setView('[obo-content]', view);
                view.render();
            }
        });
    };

    module.exports = Backbone.SubRoute.extend({
        initialize: function(){
            this.model = new Model();
            this.stopListening(eventAggregator,'getCustomerModel');
            this.listenTo(eventAggregator, 'getCustomerModel', function(callback){
                var id = commonFunction.getUrlHashSplit(3);
                if (!this.model.id || (this.model.id != id)){
                    this.model.set(this.model.idAttribute, id);
                    this.model.once('sync', function(model){
                        callback(model);
                    });
                    this.model.fetch();
                }else{
                    callback(this.model);
                }
            });

        },
        routes: {
            '': 'redirectToCustomerProfile',
            'customer_profile': 'showCustomerProfile',
            'customer_profile/edit': 'showCustomerProfileEdit',
            'products': 'showProducts',
            'products/detail/:id': 'showProductsDetail',
            'orders': 'showOrders',
            'orders/detail/:id': 'showOrdersDetail',
            'invoices': 'showInvoices',
            'invoices/detail/:id': 'showInvoicesDetail',
            'pods': 'showPODs',
            'pods/detail/:id': 'showPODsDetail',
            'addresses': 'showAddresses',
            'addresses/add': 'showAddressesAdd',
            'addresses/detail/:id': 'showAddressesDetail',
            'addresses/detail/:id/edit': 'showAddressesDetailEdit',
            'contacts': 'showContacts',
            'contacts/add': 'showContactsAdd',
            'contacts/detail/:id': 'showContactsDetail',
            'contacts/detail/:id/edit': 'showContactsDetailEdit',
            'sender_and_receivers': 'showSenderAndReceiver',
            'sender_and_receivers/add': 'showSenderAndReceiverAdd',
            'sender_and_receivers/detail/:id': 'showSenderAndReceiverDetail',
            'sender_and_receivers/detail/:id/edit': 'showSenderAndReceiverDetailEdit'
        },
        redirectToCustomerProfile: function(){
            this.navigateBackbonePrototype(commonFunction.getCurrentHashToLevel(3).replace('#', '') + '/customer_profile', {trigger: true, replace: true});
        },
        showCustomerProfile: function() {
            fnSetContentView('customer_profile');
        },
        showCustomerProfileEdit: function() {
            fnSetContentView('customer_profile/edit', 'replace');
        },
        showProducts: function() {
            fnSetContentView('products');
        },
        showProductsDetail: function() {
            fnSetContentView('products/detail', 'replace');
        },
        showOrders: function() {
            fnSetContentView('orders');
        },
        showOrdersDetail: function(){
            fnSetContentView('orders/detail', 'replace');
        },
        showInvoices: function() {
            fnSetContentView('invoices');
        },
        showInvoicesDetail : function(){
            fnSetContentView('invoices/detail','replace');
        },
        showPODs: function(id) {
            fnSetContentView('pods');
        },
        showPODsDetail: function(id) {
            fnSetContentView('pods/detail','replace');
        },
        showAddresses: function() {
            fnSetContentView('addresses');
        },
        showAddressesAdd: function() {
            fnSetContentView('addresses/add', 'replace');
        },
        showAddressesDetail: function(){
            fnSetContentView('addresses/detail', 'replace');
        },
        showAddressesDetailEdit: function() {
            fnSetContentView('addresses/detail/edit', 'replace');
        },
        showContacts: function() {
            fnSetContentView('contacts');
        },
        showContactsAdd: function() {
            fnSetContentView('contacts/add', 'replace');
        },
        showContactsDetail : function(){
          fnSetContentView('contacts/view', 'replace');
        },
        showContactsDetailEdit: function() {
            fnSetContentView('contacts/edit', 'replace');
        },
        showSenderAndReceiver: function() {
            fnSetContentView('senders_and_receivers');
        },
        showSenderAndReceiverAdd: function() {
            fnSetContentView('senders_and_receivers/add', 'replace');
        },
        showSenderAndReceiverDetail: function() {
            fnSetContentView('senders_and_receivers/detail', 'replace');
        },
        showSenderAndReceiverDetailEdit: function(){
            fnSetContentView('senders_and_receivers/detail/edit', 'replace');
        }
    });
});
