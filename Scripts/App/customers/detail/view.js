// scripts/app/customers/detail
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var eventAggregator = require('eventaggregator');

    module.exports = LayoutManager.extend({
        name: 'customers.detail',
        className: 'container-fluid main-content',
        template: _.template(template),
        afterRender: function() {
            var View = require('./menu/view');
            var view = new View();

            this.insertView('[obo-menu]', view);
            view.render();
            eventAggregator.trigger('getCustomerModel', function(model){
                self.$('.header > div > [name="CustomerName"]').text(model.toJSON().CustomerName);
            });
        }
    });
});
