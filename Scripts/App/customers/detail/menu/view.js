// scripts\app\customers\detail\menu
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        className: 'submenu-content',
        template: _.template(template),
        afterRender: function() {
            this.doActiveButton();
        },
        doActiveButton: function() {
            var fragmentHash = window.location.hash.split('/')[3];
            var classActive = 'bs-callout-active';
            this.$('a').removeClass(classActive);

            if (fragmentHash) {
                var buttonIsActive = fragmentHash.replace(/\?(.*)/ig, '');
                this.$('a[href$=' + buttonIsActive + ']').addClass(classActive);
            }
        }
    });
});
