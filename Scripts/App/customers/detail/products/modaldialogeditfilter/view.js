// scripts/app/customers/modaldialogeditfilter
define(function(require, exports, module) {
    'use strict';
    // var LayoutManager = require('layoutmanager');
    var View = require('modaldialogeditfilter');
    var template = require('text!./template.html');

    module.exports = View.extend({
        template: _.template(template)
    });
});
