// scripts/app/customers/detail/products/filter
define(function(require, exports, module) {
    'use strict';
    var Filter = require('filter');
    var template = require('text!./template.html');
    var commonConfig = require('commonconfig');
    var commonFunction = require('commonfunction');
    var Cookies = require('Cookies');
    require('select2');

    module.exports = Filter.extend({
        className: '',
        template: _.template(template),
        defaultShowFields: ['ProductName', 'ProductCode', 'CustomerID', 'IsDangerousGoods', 'UnitValue'],
        initialize: function() {
            var self = this;
            this.fieldsShowed = this.defaultShowFields.slice();
            commonFunction.setSelect2IsDangerousGoods(this);
            commonFunction.setSelect2CustomerAccountOwner(this, {
                selector: '[name="AccountName"]',
                placeholder: 'Account Owner'
            });

            commonFunction.setSelect2ProductProductCategory(this, {
                placeholder: 'Product Category'
            });

            commonFunction.setSelect2ProductManufactureLeadTime(this, {
                placeholder: 'Manufacturer Lead Time'
            });
            commonFunction.setSelect2ProductSKUType(this, {
                placeholder: 'SKU Type'
            });

            commonFunction.setSelect2ProductPackUnit(this, {
                placeholder: 'Product Pack Unit'
            })


            this.on('beforeRender cleanup', function() {
                var selectorClass = '.select2-hidden-accessible';
                if (this.$(selectorClass + '[name="CustomerID"]').length) {
                    this.$(selectorClass + '[name="CustomerID"]').select2('destroy');
                }

                if (this.$(selectorClass + '[name="IsTemperatureSensitive"]').length) {
                    this.$(selectorClass + '[name="IsTemperatureSensitive"]').select2('destroy');
                }

                _.each(['CustomerID', 'IsTemperatureSensitive', 'IsPerishableGoods'], function(fieldName) {
                    if (self.$(selectorClass + '[name="' + fieldName + '"]').length) {
                        self.$(selectorClass + '[name="' + fieldName + '"]').select2('destroy');
                    }
                })
            });

            this.on('afterRender', function() {
                this.$('[name="CustomerID"]').select2({
                    minimumInputLength: 1,
                    placeholder: 'Customer',
                    ajax: {
                        headers: {
                            'Authorization': Cookies.get(commonConfig.cookieFields.Authorization)
                        },
                        url: commonConfig.requestServer + 'Customer',
                        dataType: 'json',
                        type: "GET",
                        quietMillis: 50,
                        data: function(params) {
                            var queryParameters = {
                                DisplayRows: 100,
                                Page: 1,
                                OrderBy: '',
                                FilterParams: [{
                                    Key: 'CustomerName',
                                    Value: params.term
                                }]
                            }
                            return queryParameters;
                        },
                        processResults: function(data) {
                            return {
                                results: $.map(data.ResultList, function(item) {
                                    return {
                                        text: item.CustomerName,
                                        id: item.CustomerID
                                    }
                                })
                            };
                        }
                    }
                });

                this.$('[name="CustomerID"]').on('select2:select', function() {});
                this.$('[name="IsTemperatureSensitive"]').select2();
                this.$('[name="IsPerishableGoods"]').select2();
                this.$('[name="IsFragileGoods"]').select2();

            });
        }
    });
});
