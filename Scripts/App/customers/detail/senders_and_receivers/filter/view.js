// scripts/app/customers/detail/senders_and_recievers/filter
define(function(require, exports, module) {
    'use strict';
    var Filter = require('filter');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');

    module.exports = Filter.extend({
        tagName: 'form',
        className: 'row',
        template: _.template(template),
        defaultShowFields: ['FirstName', 'LastName', 'Country', 'AccountCode', 'IsActive'],
        initialize: function() {
            this.fieldsShowed = this.defaultShowFields.slice();

            commonFunction.setSelect2GeneralCountry(this, {
                placeholder: 'Country'
            });

            commonFunction.setSelect2Status(this, {
                selector: '[name="IsActive"]',
                placeholder: 'Status'
            });
        }
    });
});
