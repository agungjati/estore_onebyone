﻿
define(function(require, exports, module) {
    'use strict';

    var commonConfig = require('commonconfig');
    var commonFunction = require('commonfunction');


    if (commonFunction.isLoginsHash()) {
        module.exports = function(){
            return {
                Start: function(callback){
                    callback();
                }
            }
        }
    } else {
        var Tether = require('tether');
        require('backbone');

        var SideBarView = require('./layout/sidebar/view');
        var NavBarView = require('./layout/navbar/view');
        var ContentView = require('./layout/content/view');

        var contentView = new ContentView();
        var navBarView = new NavBarView();
        var sideBarView = new SideBarView();

        commonFunction.setContentView(contentView);
        commonFunction.setSideBarView(sideBarView);
        commonFunction.setNavBarView(navBarView);

        requirejs(['simplebar', 'bootstrap-layout', 'bootstrap-layout-scrollable'], function() {});
        $('div.contents', document.body).prepend(contentView.render().el);
        $('body.app').prepend(sideBarView.render().el);
        $('body.app').prepend(navBarView.render().el);

        module.exports = function() {
            return {
                Start: function(callback) {
                    callback(contentView);
                }
            };
        };
    };
});
