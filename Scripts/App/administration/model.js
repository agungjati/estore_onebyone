define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'WarehouseID',
        //urlRoot: 'dummydata/Warehouse',
        urlRoot: 'Storage/Warehouse',
        defaults: function() {
            return {
                CustomerNo: '',
                CustomerName: '',
                CustomerID: '',
                CustomerTradingAs: '',
                AccountOwner: {
                    AccountNo: '',
                    AccountNmae: '',
                    IsActive: ''
                },
                CustomerABN: '',
                CustomerACN: '',
                AccountNo:'',
                DateStart: '',
                ListCustomerTag: '',
                Phone: '',
                Mobile: '',
                Fax: '',
                CreditLimit: '',
                InvoiceEmail: '',
                FreightPayable: {
                    FreightPayableName: '',
                    IsActive: ''
                },
                PodEmail: '',
                IsActive: '',
                ListCustomerTag:[],
                PaymentTerm:{
                    PaymentTermID: '',
                    PaymentTermName:''
                },
                WarehouseOwner:{
                    AccountName:'',
                },
                WarehouseName: '',
                Address1: '',
                Country:{
                    CuntryName: ''
                },
                PostCode: '',
                State: '',
                City: '',
                Email: '',
                WarehouseManagementType: {
                    WarehouseManagementTypeName: ''
                },
                WarehouseDescription: '',
                WarehouseManager: {
                    WarehouseManagerName: ''
                }
            }
        }
    });
});
