﻿
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var BModel = require('backbone.model');
    var BCollection = require('backbone.collection');
    var commonConfig = require('commonconfig');
    var Cookies = require('Cookies');
    require('select2');
    require('sweetalert');

    var _this = {
        collection: {}
    };

    var select2Option = {
        isPlaceholder: true,
        noRedefineSelect2: false,
        getOptionValue: function(model) {
            return model.id
        }
    }

    var aryYesNo = [{
        value: 'true',
        text: 'Yes'
    }, {
        value: 'false',
        text: 'No'
    }];

    Number.prototype.toFixedbackup = Number.prototype.toFixed;
    Number.prototype.toFixed = function(precision) {
        // BEWARE: Below method has been enhanced.
        // Dont try to modify if u dont understand. Peace ;) - JL -
        var str = Math.abs(this).toString(),
            negative = this < 0,
            precisionLength = (str.indexOf('.') === -1) ? 0 : str.length - str.indexOf('.') - 1,
            lastNumber, mult;
        str = str.substr(0, (str.indexOf('.') === -1) ? str.length : (str.indexOf('.') + precision + 2));
        lastNumber = str.charAt(str.length - 1);

        if (lastNumber >= 5 && precision < precisionLength) {
            str = str.substr(0, str.length - 1);
            mult = Math.pow(10, str.length - str.indexOf('.') - 1);
            str = (+str + 1 / mult);
        }
        return (negative ? "-" : "") + (str * 1).toFixedbackup(precision);
    };

    var setSelect2 = function(self, view, options) {

        return new Promise(function(resolve, reject) {

            var collection = options.getCollection();
            var selector = options.selector;

            var createSelect2 = function() {
                if (view.$(selector).length && !view.$(selector).hasClass('select2-hidden-accessible')) {
                    if (!options.noRedefineSelect2) {
                        var opt = {};
                        if (options.isPlaceholder === true) {
                            opt.placeholder = options.placeholder;
                        }
                        view.$(selector).select2(opt);
                    }
                }
            };

            view.listenTo(collection, 'sync', function() {
                this.$(selector).empty().append(new Option());

                collection && _.each(collection.models, function(model) {
                    var option = new Option(options.getOptionText(model), options.getOptionValue(model));
                    if (options.newOptionHtml) {
                        option = options.newOptionHtml(options, model, option);
                    }

                    this.$(selector).append(option);
                }, view);

                resolve({
                    self: self,
                    view: view,
                    options: options,
                    collection: collection
                });
            });

            view.on('beforeRender', function() {
                if (!options.noRedefineSelect2) {
                    this.$(selector).select2('destroy');
                }
            });

            view.on('afterRender', function() {
                if (!collection.length) {
                    collection.fetch();
                } else {
                    collection.trigger('sync');
                }
                createSelect2();
            });

            view.on('beforeRemove cleanup', function() {
                if (!options.noRedefineSelect2) {
                    this.$(selector).select2('destroy');
                }
            });

            createSelect2();
        });
    };

    var createCollection = function(name, idAttribute, url) {
        if (!_this.collection[name]) {
            var Model = BModel.extend({
                idAttribute: idAttribute
            });
            var Collection = BCollection.extend({
                model: Model
            });

            _this.collection[name] = new Collection();
            _this.collection[name].url = url;
        }
        return _this.collection[name];
    }

    module.exports = {
        setContentView: function(view) {
            _this.contentView = view;
        },
        getContentView: function() {
            return _this.contentView;
        },
        setSideBarView: function(view) {
            _this.sideBarView = view;
        },
        getSideBarView: function() {
            return _this.sideBarView;
        },
        setNavBarView: function(view) {
            _this.navBarView = view;
        },
        getNavBarView: function() {
            return _this.navBarView;
        },
        setLoginView: function(view) {
            _this.loginView = view;
        },
        getLoginView: function() {
            return _this.loginView;
        },
        setContentViewWithNewModuleView: function(newModuleView, moduleActivedByUrl) {
            var self = this;
            var view = undefined;

            var fnRemoveViews = function() {
                var contentView = self.getContentView();
                if (contentView) {
                    var oldModuleView = contentView.getView('');
                    if (oldModuleView) {
                        contentView.removeView('');
                        oldModuleView.remove();
                    }
                    contentView.remove();
                    self.setContentView();
                }

                var sideBarView = self.getSideBarView();
                if (sideBarView) {
                    sideBarView.remove();
                    self.setSideBarView();
                }

                var navBarView = self.getNavBarView();
                if (navBarView) {
                    navBarView.remove();
                    self.setNavBarView();
                }
            }

            var fnRemoveLoginViews = function() {
                view = self.getLoginView();
                view && view.remove();
            }

            if (this.isLoginsHash()) {
                fnRemoveViews()

                if ($('body').hasClass('app')) {
                    $('body').removeAttr('class').css('background-size', 'cover')
                    $('body').attr('background', '/Images/log_loginbackground.png');
                    $('body > .layout-content').remove();
                    $('body').append('<div class="login"></div>');
                }

                if (newModuleView) {
                    view = self.getLoginView();
                    view && view.remove();
                    view = newModuleView;
                    self.setLoginView(view);

                    $('body').removeAttr();
                    $('body > .login').append(view.el);
                    view.render();
                }

            } else {
                fnRemoveLoginViews();

                if ($('body > .login').length) {
                    $('body > .login').remove();
                    $('body').removeAttr('background').removeAttr('style');
                }

                var SideBarView = require('./layout/sidebar/view');
                var NavBarView = require('./layout/navbar/view');
                var ContentView = require('./layout/content/view');

                var contentView = self.getContentView();
                var navBarView = self.getNavBarView();
                var sideBarView = self.getSideBarView();

                if (!navBarView) {
                    navBarView = new NavBarView();
                    self.setNavBarView(navBarView);
                }

                if (!sideBarView) {
                    sideBarView = new SideBarView();
                    self.setSideBarView(sideBarView);
                }
                if (!contentView) {
                    contentView = new ContentView();
                    self.setContentView(contentView);
                }

                if (!$('body').hasClass('app')) {
                    $('body').addClass('app layout-container ls-top-navbar layout-sidebar-l3-md-up').append('<div class="layout-content" data-scrollable> <div class="container-fluid contents"></div></div>');

                    requirejs(['simplebar', 'bootstrap-layout', 'bootstrap-layout-scrollable'], function() {});
                    $('div.contents', document.body).prepend(contentView.render().el);
                    $('body.app').prepend(sideBarView.render().el);
                    $('body.app').prepend(navBarView.render().el);
                }


                var oldModuleView = contentView.getView('');
                if (oldModuleView) {
                    contentView.removeView('');
                    oldModuleView.remove();
                }

                contentView.setView('', newModuleView);
                newModuleView.render();

                sideBarView.$('.sidebar-menu li a.active').each(function(index, item) {
                    var domImg = $('img', item);
                    var src = domImg.attr('src') || '';
                    domImg.attr('src', src.replace('_primary.png', '.png'));
                });

                sideBarView.$('.sidebar-menu li a.active').removeClass('active');

                if (moduleActivedByUrl) {
                    var linkFound = sideBarView.$('.sidebar-menu > li  a[href="' + moduleActivedByUrl + '"]');

                    if (linkFound) {
                        linkFound.addClass('active');
                        if (linkFound.parents('li').length) {
                            var length = linkFound.parents('li').length - 1;
                            var link = linkFound.parents('li')[length];
                            $('> a', link).parent().find('> a').addClass('active');
                            var domImg = $('img', link);
                            var src = domImg.attr('src') || '';
                            domImg.attr('src', src.replace('.png', '_primary.png'));
                        }
                    }
                }
            }
        },
        dateFromUTCToLocalFormated: function(dateUTC) {
            var date = moment.utc(dateUTC);
            date = moment(date).local();
            return date.format(commonConfig.momentFormat);
        },
        autoNumericInit: function(item) {
            item.autoNumeric('init', {
                vMin: 0,
                vMax: 99999999999,
                mDec: 0,
                aPad: false,
                wEmpty: 'zero'
            });
        },
        autoNumericInitAccept2Digits: function(item) {
            item.autoNumeric('init', {
                vMin: 0,
                vMax: 99999999999,
                aPad: false,
                mDec: 2,
                wEmpty: 'zero'
            });
        },
        autoNumericInitAccept2DigitsPercent: function(item) {
            item.autoNumeric('init', {
                vMin: 0,
                vMax: 99999999999,
                aPad: false,
                mDec: 2,
                aSign: ' %',
                pSign: 's',
                wEmpty: 'zero'
            });
        },
        autoNumericInitAccept2DigitsPercentLimit100: function(item) {
            item.autoNumeric('init', {
                vMin: 0,
                vMax: 100,
                aPad: false,
                mDec: 2,
                aSign: ' %',
                pSign: 's',
                wEmpty: 'zero'
            });
        },
        autoNumericInitAcceptPercent5Digits: function(item) {
            item.autoNumeric('init', {
                aPad: false,
                mDec: 5,
                pSign: 's',
                aSign: ' %',
                vMin: 0,
                vMax: 100,
                wEmpty: 'zero'
            });
        },
        autoNumericInitAcceptPercent3Digits: function(item) {
            item.autoNumeric('init', {
                aPad: false,
                mDec: 3,
                pSign: 's',
                aSign: ' %',
                vMin: 0,
                vMax: 100,
                wEmpty: 'zero'
            });
        },
        autoNumericInitAcceptPercent: function(item) {
            item.autoNumeric('init', {
                aPad: false,
                mDec: 0,
                pSign: 's',
                aSign: ' %',
                vMin: 0,
                vMax: 100,
                wEmpty: 'zero'
            });
        },
        autoNumericInitAcceptPercentAllow100: function(item) {
            item.autoNumeric('init', {
                aPad: false,
                mDec: 0,
                pSign: 's',
                aSign: ' %',
                vMin: 0,
                vMax: 99999999999,
                wEmpty: 'zero'
            });
        },
        autoNumericInitAcceptPercent3DigitsAllow100: function(item) {
            item.autoNumeric('init', {
                aPad: false,
                mDec: 3,
                pSign: 's',
                aSign: ' %',
                vMin: 0,
                vMax: 99999999999,
                wEmpty: 'zero'
            });
        },
        modelCustomer: function() {

        },
        getURLParamsString: function() {
            return window.location.hash.split('/?')[1];
        },
        getURLParamsObj: function() {
            var param = this.getURLParamsString();
            if (param)
                return JSON.parse('{"' + decodeURI(param).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}');
            else
                return undefined;
        },
        getUrlParameter: function(field) {
            field = field.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            var regexS = "[\\?&]" + field + "=([^&#]*)";
            var regex = new RegExp(regexS);
            var results = regex.exec(location.href);
            return results == null ? null : results[1];
        },
        getUrlHashSplit: function(index) {
            var aryHash = window.location.hash.split('/');
            if (index)
                return aryHash[--index];
            return aryHash;
        },
        getCurrentHash: function() {
            var length = window.location.hash.split('/').length;
            return window.location.hash.split('/').slice(0, length).join('/')
        },
        getCurrentHashToLevel: function(level) {
            if (level) {
                return window.location.hash.split('/').slice(0, level).join('/');
            }
            return '';
        },
        getCurrentHashOmitSuffix: function(amountRemoveSuffix) {
            if (amountRemoveSuffix) {
                var length = window.location.hash.split('/').length;
                var totalHashChunk = length - amountRemoveSuffix;
                return window.location.hash.split('/').slice(0, totalHashChunk).join('/')
            }
        },
        getLastSplitHash: function() {
            var length = window.location.hash.split('/').length;
            var getLasthSplitHashIndex = --length;
            return window.location.hash.split('/')[getLasthSplitHashIndex];
        },
        loadModule: function(pathFile) {
            var pathFiles = [];
            if (typeof pathFile == 'string') {
                pathFiles.push(pathFile);
            } else {
                pathFiles = pathFile;
            }

            if (!window.isLoadingModule) {
                window.isLoadingModule = true;
                require(pathFiles, function() {
                    window.isLoadingModule = false;
                });
            }
        },
        checkCookieAuthorization: function() {
            var self = this;
            var roleName = Cookies.get(commonConfig.cookieFields.roleName);
            var userName = Cookies.get(commonConfig.cookieFields.userName);

            // not yet check to server about token bearer ... it is a must
            return new Promise(function(resolve, reject) {

                if (roleName && userName) {
                    resolve();
                } else {
                    if (self.isLoginsHash()) {
                        resolve();
                    } else {
                        reject();
                    }
                }
            })
        },
        responseStatusNot200: function(object) {

            object = object || {};
            // var title = object.title || 'Cannot Connect to Server.';
            // var text = object.text || 'Please Inform Our Technical Support, Support@onebyonedigital.com and told us what the last activity';
            var title = object.title || 'Ops!. we got a problem here';
            var text = object.text || '';
            var xhr = object.xhr || '';

            var responseJSON = xhr.responseJSON;
            var msg = ' ';

            if (responseJSON) {
                //msg += '<br> server response : ' + (responseJSON.Message || responseJSON.error_description || responseJSON.error || responseJSON.responseText) + ' ';
                msg += (responseJSON.Message || responseJSON.error_description || responseJSON.error || responseJSON.responseText || responseJSON);
            } else {
                if (xhr.responseText) {
                    msg += xhr && xhr.responseText;
                } else if (xhr.status == 0) {
                    msg += ('unknown error occured. server response not received.');
                }
            }
            msg += ' <br> if need help, please contact to Support@onebyonedigital.com';

            //title = typeof title == 'undefined' ? 'Warning' : title;
            //text = typeof text == 'undefined' ? 'sorry, something went wrong.' : text;
            swal({
                title: "<span class='text-danger'>" + title + "</span>",
                text: text + ' ' + msg,
                html: true
            });
            //swal("Here's a message!", "It's pretty, isn't it?");
            // window.alert('server is down : please tell to logistical maintenance team and the last time what you did... ' + msg);
        },
        formDataToJson: function(data) {
            return _.object(_.pluck(data, 'name'), _.pluck(data, 'value'));
        },
        doIfLocalhost: function(fn) {
            if (window.location.hostname == 'localhost')
                fn();
        },
        isLoginsHash: function() {
            return _.find(commonConfig.aryLogin, function(hash) {
                return location.href.split(/\?|#/)[1] == hash;
            });
        },
        getCollectionCustomerAccountOwner: function() {
            if (!_this.collection.accountowner) {

                var Model = BModel.extend({
                    idAttribute: 'AccountOwnerID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.accountowner = new Collection();
                _this.collection.accountowner.url = 'Customer/AccountOwner';
            }
            return _this.collection.accountowner
        },
        setSelect2CustomerAccountOwner: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="AccountOwner"]',
                    placeholder: 'Select Account Owner',
                    getCollection: self.getCollectionCustomerAccountOwner,
                    getOptionText: function(model) {
                        return model.get('AccountName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },
        getCollectionCustomerPaymentTerm: function() {
            if (!_this.collection.customerPaymentTerm) {

                var Model = BModel.extend({
                    idAttribute: 'PaymentTermID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.customerPaymentTerm = new Collection();
                _this.collection.customerPaymentTerm.url = 'Customer/PaymentTerm';
            }
            return _this.collection.customerPaymentTerm
        },
        setSelect2CustomerPaymentTerm: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="PaymentTermID"]',
                    placeholder: 'Select Payment Term',
                    getCollection: self.getCollectionCustomerPaymentTerm,
                    getOptionText: function(model) {
                        return model.get('PaymentTermName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });

        },
        getCollectionCustomerTags: function() {
            if (!_this.collection.customerTags) {

                var Model = BModel.extend({
                    idAttribute: 'TagID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.customerTags = new Collection();
                _this.collection.customerTags.url = 'Customer/Tags';
            }
            return _this.collection.customerTags
        },
        setSelect2CustomerTags: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {

                var defaultOption = {
                    selector: '[name="Tags"]',
                    placeholder: 'Select Tag',
                    getCollection: self.getCollectionCustomerTags,
                    getOptionText: function(model) {
                        return model.get('TagName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });

        },
        getCollectionGeneralCurrencies: function() {
            if (!_this.collection.generalCurrencies) {

                var Model = BModel.extend({
                    idAttribute: 'CurrencyCode'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.generalCurrencies = new Collection();
                _this.collection.generalCurrencies.url = 'General/Currencies';
            }
            return _this.collection.generalCurrencies;
        },
        setSelect2GeneralCurrencies: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="Currency"]',
                    placeholder: 'Select Currency',
                    getCollection: self.getCollectionGeneralCurrencies,
                    getOptionText: function(model) {
                        return model.get('CurrencyCode');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },
        getCollectionCustomerFreightPayable: function() {
            if (!_this.collection.customerFreightPayable) {

                var Model = BModel.extend({
                    idAttribute: 'FreightPayableID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.customerFreightPayable = new Collection();
                _this.collection.customerFreightPayable.url = 'Customer/FreightPayable';
            }
            return _this.collection.customerFreightPayable;
        },
        setSelect2CustomerFreightPayable: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="FreightPayableID"]',
                    placeholder: 'Select Freight Payable',
                    getCollection: self.getCollectionCustomerFreightPayable,
                    getOptionText: function(model) {
                        return model.get('FreightPayableName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });

        },

        //get List Product Category
        getCollectionProductProductCategory: function() {
            // if (!_this.collection.productProductCategory) {
            //
            //     var Model = BModel.extend({
            //         idAttribute: 'ProductCategoryID'
            //     });
            //
            //     var Collection = BCollection.extend({
            //         model: Model
            //     });
            //     _this.collection.productProductCategory = new Collection();
            //     _this.collection.productProductCategory.url = 'Product/ProductCategory';
            // }
            // return _this.collection.productProductCategory;
            return createCollection('productProductCategory', 'ProductCategoryID', 'Product/ProductCategory');
        },
        setSelect2ProductProductCategory: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="ProductCategory"]',
                    placeholder: 'Select Product Category',
                    getCollection: self.getCollectionProductProductCategory,
                    getOptionText: function(model) {
                        return model.get('ProductCategoryName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });

        },
        //get AddressType
        getCollectionAddressType: function() {
            if (!_this.collection.addressType) {

                var Model = BModel.extend({
                    idAttribute: 'AddressTypeID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.addressType = new Collection();
                _this.collection.addressType.url = 'Customer/AddressType';
            }
            return _this.collection.addressType;
        },
        setSelect2AddressType: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="AddresstypeID"]',
                    placeholder: 'Select Address Type',
                    getCollection: self.getCollectionAddressType,
                    getOptionText: function(model) {
                        return model.get('AddressTypeName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });

        },

        //get List State
        getCollectionState: function() {
            if (!_this.collection.generalState) {

                var Model = BModel.extend({
                    idAttribute: 'StateID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.generalState = new Collection();
                _this.collection.generalState.url = 'General/State';
            }
            return _this.collection.generalState;
        },
        setSelect2GeneralState: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="State"]',
                    placeholder: 'Select State',
                    getCollection: self.getCollectionState,
                    getOptionText: function(model) {
                        return model.get('StateName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });

        },
        //get List Country
        getCollectionCountry: function() {
            if (!_this.collection.generalCountry) {

                var Model = BModel.extend({
                    idAttribute: 'CountryID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });

                _this.collection.generalCountry = new Collection();
                _this.collection.generalCountry.url = 'General/Country';
            }
            return _this.collection.generalCountry;
        },
        setSelect2GeneralCountry: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="Country"]',
                    placeholder: 'Select Country',
                    getCollection: self.getCollectionCountry,
                    getOptionText: function(model) {
                        return model.get('CountryName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },
        //get List City
        getCollectionCity: function() {
            if (!_this.collection.generalCity) {

                var Model = BModel.extend({
                    idAttribute: 'CitiesID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.generalCity = new Collection();
                _this.collection.generalCity.url = 'General/Cities';
            }
            return _this.collection.generalCity;
        },
        setSelect2GeneralCity: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {

                var defaultOption = {
                    selector: '[name="City"]',
                    placeholder: 'Select City',
                    getCollection: self.getCollectionCity,
                    getOptionText: function(model) {
                        return model.get('CitiesName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },
        //get List City
        getCollectionContactType: function() {
            if (!_this.collection.contactType) {

                var Model = BModel.extend({
                    idAttribute: 'ContactTypeID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.contactType = new Collection();
                _this.collection.contactType.url = 'Customer/ContactType';
            }
            return _this.collection.contactType;
        },
        setSelect2ContactType: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {

                var defaultOption = {
                    selector: '[name="ContactTypeID"]',
                    placeholder: 'Select Contact Type',
                    getCollection: self.getCollectionContactType,
                    getOptionText: function(model) {
                        return model.get('ContactTypeName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },

        getCollectionSKUType: function() {
            // if (!_this.collection.skutype) {
            //
            //     var Model = BModel.extend({
            //         idAttribute: 'SKUTypeID'
            //     });
            //
            //     var Collection = BCollection.extend({
            //         model: Model
            //     });
            //     _this.collection.skutype = new Collection();
            //     _this.collection.skutype.url = 'Product/SKUType';
            // }
            // return _this.collection.skutype
            return createCollection('skuType', 'SKUTypeID', 'Product/SKUType');
        },
        setSelect2ProductSKUType: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="SKUTypeID"]',
                    placeholder: 'Select SKU Type',
                    getCollection: self.getCollectionSKUType,
                    getOptionText: function(model) {
                        return model.get('SKUTypeName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },
        //ProductPackUnit
        getCollectionStorageChargePer: function() {
            if (!_this.collection.storageChargePer) {

                var Model = BModel.extend({
                    idAttribute: 'StorageChargePerID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.storageChargePer = new Collection();
                _this.collection.storageChargePer.url = 'Product/StorageChargePer';
            }
            return _this.collection.storageChargePer
        },
        setSelect2StorageChargePer: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="StorageChargePer"]',
                    placeholder: 'Select Storage Charge Per',
                    getCollection: self.getCollectionStorageChargePer,
                    getOptionText: function(model) {
                        return model.get('StorageChargePerName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },

        getCollectionProductSubRisk: function() {
            if (!_this.collection.subRisk) {

                var Model = BModel.extend({
                    idAttribute: 'SubRiskID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.subRisk = new Collection();
                _this.collection.subRisk.url = 'Product/SubRisk';
            }
            return _this.collection.subRisk;
        },
        setSelect2ProductSubRisk: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="DangerousSubRisk"]',
                    placeholder: 'Select Sub Risk',
                    getCollection: self.getCollectionProductSubRisk,
                    getOptionText: function(model) {
                        return model.get('SubRiskName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });

        },

        getCollectionProductUNNumber: function() {
            if (!_this.collection.productUNNumber) {

                var Model = BModel.extend({
                    idAttribute: 'UNNumberID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.productUNNumber = new Collection();
                _this.collection.productUNNumber.url = 'Product/UNNumber';
            }
            return _this.collection.productUNNumber;
        },
        setSelect2ProductUNNumber: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="DangerousUNNumber"]',
                    placeholder: 'Select UN Number',
                    getCollection: self.getCollectionProductUNNumber,
                    getOptionText: function(model) {
                        return model.get('UNNumberName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });

        },

        getCollectionProductHazchem: function() {
            if (!_this.collection.productHazchem) {

                var Model = BModel.extend({
                    idAttribute: 'HazchemID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.productHazchem = new Collection();
                _this.collection.productHazchem.url = 'Product/Hazchem';
            }
            return _this.collection.productHazchem;
        },
        setSelect2ProductHazchem: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="DangerousHazchem"]',
                    placeholder: 'Select Hazchem',
                    getCollection: self.getCollectionProductHazchem,
                    getOptionText: function(model) {
                        return model.get('HazchemName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });

        },

        getCollectionProductChemical: function() {
            if (!_this.collection.productChemical) {

                var Model = BModel.extend({
                    idAttribute: 'ChemicalID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.productChemical = new Collection();
                _this.collection.productChemical.url = 'Product/Chemical';
            }
            return _this.collection.productChemical;
        },
        setSelect2ProductChemical: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="DangerousChemicalName"]',
                    placeholder: 'Select Chemical',
                    getCollection: self.getCollectionProductChemical,
                    getOptionText: function(model) {
                        return model.get('ChemicalName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });

        },

        getCollectionProductClassProduct: function() {
            if (!_this.collection.productClassProduct) {

                var Model = BModel.extend({
                    idAttribute: 'ClassProductSubID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.productClassProduct = new Collection();
                _this.collection.productClassProduct.url = 'Product/ClassProduct';
            }
            return _this.collection.productClassProduct;
        },
        setSelect2ProductClassProduct: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="ClassProduct"]',
                    placeholder: 'Select Class Product',
                    getCollection: self.getCollectionProductClassProduct,
                    getOptionText: function(model) {
                        return model.get('ClassProductSubName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });

        },

        getCollectionProductManufactureLeadTime: function() {
            // if (!_this.collection.productManufactureLeadTime) {
            //
            //     var Model = BModel.extend({
            //         idAttribute: 'ManufactureLeadTimeID'
            //     });
            //
            //     var Collection = BCollection.extend({
            //         model: Model
            //     });
            //     _this.collection.productManufactureLeadTime = new Collection();
            //     _this.collection.productManufactureLeadTime.url = 'Product/ManufactureLeadTime';
            // }
            // return _this.collection.productManufactureLeadTime;
            return createCollection('productManufactureLeadTime', 'ManufactureLeadTimeID', 'Product/ManufactureLeadTime');
        },
        setSelect2ProductManufactureLeadTime: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="ManufactureLeadTimeID"]',
                    placeholder: 'Select Manufacture Lead Time',
                    getCollection: self.getCollectionProductManufactureLeadTime,
                    getOptionText: function(model) {
                        return model.get('ManufactureLeadTimeName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });

        },

        setSelect2Status: function(view, options) {
            var Model = BModel.extend({
                idAttribute: 'value'
            });

            var Collection = BCollection.extend({
                model: Model
            });

            var collection = new Collection([{
                value: 'true',
                text: 'Active'
            }, {
                value: 'false',
                text: 'Not Active'
            }]);

            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="Status"]',
                    placeholder: 'Select Status',
                    getCollection: function() {
                        return collection
                    },
                    getOptionText: function(model) {
                        return model.get('text');
                    }
                }

                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },

        setSelect2StatusStorageLocation: function(view, options) {
            var Model = BModel.extend({
                idAttribute: 'value'
            });

            var Collection = BCollection.extend({
                model: Model
            });

            var collection = new Collection([{
                value: 'true',
                text: 'Available'
            }, {
                value: 'false',
                text: 'Unavailable'
            }]);

            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="StatusStorageLocation"]',
                    placeholder: 'Select Status',
                    getCollection: function() {
                        return collection
                    },
                    getOptionText: function(model) {
                        return model.get('text');
                    }
                }

                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },
        setSelect2IsDangerousGoods: function(view, options) {
            var Model = BModel.extend({
                idAttribute: 'value'
            });

            var Collection = BCollection.extend({
                model: Model
            });

            var collection = new Collection(aryYesNo.slice(0));

            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="IsDangerousGoods"]',
                    placeholder: 'Dangerous Goods',
                    getCollection: function() {
                        return collection
                    },
                    getOptionText: function(model) {
                        return model.get('text');
                    }
                }

                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },

        setSelect2IsFragileGoods: function(view, options) {
            var Model = BModel.extend({
                idAttribute: 'value'
            });

            var Collection = BCollection.extend({
                model: Model
            });

            var collection = new Collection(aryYesNo.slice(0));

            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="IsFragileGoods"]',
                    placeholder: 'Fragile Goods',
                    getCollection: function() {
                        return collection
                    },
                    getOptionText: function(model) {
                        return model.get('text');
                    }
                }

                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },

        setSelect2IsPerishableGoods: function(view, options) {
            var Model = BModel.extend({
                idAttribute: 'value'
            });

            var Collection = BCollection.extend({
                model: Model
            });

            var collection = new Collection(aryYesNo.slice(0));

            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="IsPerishableGoods"]',
                    placeholder: 'Perishable Goods',
                    getCollection: function() {
                        return collection
                    },
                    getOptionText: function(model) {
                        return model.get('text');
                    }
                }

                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },

        setSelect2IsTemperatureSensitive: function(view, options) {
            var Model = BModel.extend({
                idAttribute: 'value'
            });

            var Collection = BCollection.extend({
                model: Model
            });

            var collection = new Collection([{
                value: 'true',
                text: 'Yes'
            }, {
                value: 'false',
                text: 'No'
            }]);

            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="IsTemperatureSensitive"]',
                    placeholder: 'Temperature Sensitive',
                    getCollection: function() {
                        return collection
                    },
                    getOptionText: function(model) {
                        return model.get('text');
                    }
                }

                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },

        //social media
        getCollectionCustomerProviderSocialMedia: function() {
            if (!_this.collection.customerProviderSocialMedia) {

                var Model = BModel.extend({
                    idAttribute: 'ProvSocialMediaID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.customerProviderSocialMedia = new Collection();
                _this.collection.customerProviderSocialMedia.url = 'Customer/ProviderSocialMedia';
            }
            return _this.collection.customerProviderSocialMedia;
        },
        setSelect2CustomerProviderSocialMedia: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="ProviderSocialMedia"]',
                    placeholder: 'Select Social Media',
                    getCollection: self.getCollectionCustomerProviderSocialMedia,
                    getOptionText: function(model) {
                        return model.get('SocialMediaName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },
        setSelect2GoodsCondition: function(view, options) {
            var Model = BModel.extend({
                idAttribute: 'value'
            });

            var Collection = BCollection.extend({
                model: Model
            });

            var collection = new Collection([{
                value: 'true',
                text: 'Good'
            }, {
                value: 'false',
                text: 'Damaged'
            }]);

            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="GoodsConditionID"]',
                    placeholder: 'Select Goods Condition',
                    getCollection: function() {
                        return collection
                    },
                    getOptionText: function(model) {
                        return model.get('text');
                    }
                }

                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },
        setSelect2SInwardGoodstatus: function(view, options) {
            var Model = BModel.extend({
                idAttribute: 'value'
            });

            var Collection = BCollection.extend({
                model: Model
            });

            var collection = new Collection([{
                value: 'true',
                text: 'Stored'
            }, {
                value: 'false',
                text: 'Dispatch'
            }]);

            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="StatusID"]',
                    placeholder: 'Select Status',
                    getCollection: function() {
                        return collection
                    },
                    getOptionText: function(model) {
                        return model.get('text');
                    }
                }

                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },


        getCollectionStorageRefWarehouseOwner: function() {
            if (!_this.collection.storageRefWarehouseOwner) {

                var Model = BModel.extend({
                    idAttribute: 'WarehouseOwnerID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.storageRefWarehouseOwner = new Collection();
                _this.collection.storageRefWarehouseOwner.url = 'StorageRef/WarehouseOwner';
            }
            return _this.collection.storageRefWarehouseOwner;
        },
        setSelect2StorageRefWarehouseOwner: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="WarehouseOwnerID"]',
                    placeholder: 'Select Warehouse Owner',
                    getCollection: self.getCollectionStorageRefWarehouseOwner,
                    getOptionText: function(model) {
                        return model.get('WarehouseOwnerName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },

        getCollectionWarehouseManagementType: function() {
            if (!_this.collection.warehouseManagementType) {

                var Model = BModel.extend({
                    idAttribute: 'WarehouseManagementTypeID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });
                _this.collection.warehouseManagementType = new Collection();
                _this.collection.warehouseManagementType.url = 'StorageRef/WarehouseManagementType';
            }
            return _this.collection.warehouseManagementType;
        },
        setSelect2WarehouseManagementType: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="WarehouseManagementTypeID"]',
                    placeholder: 'Select Warehouse Management Type',
                    getCollection: self.getCollectionWarehouseManagementType,
                    getOptionText: function(model) {
                        return model.get('WarehouseManagementTypeName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },
        getCollectionWarehouseManager: function() {
            var name = 'warehouseManager';
            if (!_this.collection[name]) {

                var Model = BModel.extend({
                    idAttribute: 'WarehouseManagerID'
                });

                var Collection = BCollection.extend({
                    model: Model
                });

                _this.collection[name] = new Collection();
                _this.collection[name].url = 'StorageRef/WarehouseManager';
            }
            return _this.collection[name];
        },
        setSelect2WarehouseManager: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="WarehouseManagerID"]',
                    placeholder: 'Select Warehouse Manager',
                    getCollection: self.getCollectionWarehouseManager,
                    getOptionText: function(model) {
                        return model.get('WarehouseManagerName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },
        getCollectionZoneType: function() {
            return createCollection('zoneType', 'ZoneTypeID', 'StorageRef/ZoneType');
        },
        setSelect2ZoneType: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="ZoneTypeID"]',
                    placeholder: 'Select Zone Type',
                    getCollection: self.getCollectionZoneType,
                    getOptionText: function(model) {
                        return model.get('ZoneTypeName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },
        getCollectionZoneArea: function() {
            return createCollection('zoneArea', 'ZoneAreaID', 'StorageRef/ZoneArea');
        },
        setSelect2ZoneArea: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="ZoneAreaID"]',
                    placeholder: 'Select Zone Area',
                    getCollection: self.getCollectionZoneArea,
                    getOptionText: function(model) {
                        return model.get('ZoneAreaName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },
        //for example this is used for filter on customer products
        getCollectionProductPackUnit: function() {
            return createCollection('productPackUnit', 'ProductPackUnitID', 'Product/ProductPackUnit?SKUType=')
        },
        //for example this is used for filter on customer products
        setSelect2ProductPackUnit: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="ProductPackUnitID"]',
                    placeholder: 'Select Prouduct Pack Unit',
                    getCollection: self.getCollectionProductPackUnit,
                    getOptionText: function(model) {
                        return model.get('ProductPackUnitName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },

        getCollectionSourceStock: function() {
            return createCollection('sourceStock', 'SourceStockID', 'Storage/StockTransfer' + this.getUrlHashSplit(3) + '/SourceStock');
        },
        setSelect2SourceStock: function(view, options) {
            var self = this;
            return new Promise(function(resolve) {
                var defaultOption = {
                    selector: '[name="SourceStockID"]',
                    placeholder: 'Select Source Stock',
                    getCollection: self.getCollectionSourceStock,
                    getOptionText: function(model) {
                        return model.get('SourceStockName');
                    }
                }
                setSelect2(self, view, _.extend({}, select2Option, defaultOption, options)).then(resolve);
            });
        },

        showLoadingScreen: function(message) {
            message = message || 'Please Wait ...';
            if (!$('#loader-wrapper').length) {
                var dom = $('<div id="loader-wrapper">\
    						<div class="loader">\
    						<div class="text-center"><h3>' + message + '</h3></div></div>\
    					</div>');
                $('body').append(dom);
                $(dom).fadeIn();
            }
        },
        removeLoadingScreen: function() {
            if ($('#loader-wrapper').length) {
                $('#loader-wrapper').fadeOut({
                    complete: function() {
                        $('#loader-wrapper').remove();
                    }
                });
            }
        },
        getGoogleMapAutoCompleteService: function() {
            if (!this.googleMapAutoCompleteService) {
                this.googleMapAutoCompleteService = new google.maps.places.AutocompleteService();
            }
            return this.googleMapAutoCompleteService;
        },
        getGoogleMapPlacesPlaceService: function() {
            if (!this.googleMapPlacesPlaceService) {
                this.googleMapPlacesPlaceService = new google.maps.places.PlacesService(document.createElement('div'));
            }
            return this.googleMapPlacesPlaceService;
        },
        getGoogleMapDetailResult: function(places, status) {
            var aryDetailResult = Object.assign({}, commonConfig.emptyAddressInterest);
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                if (places.address_components.length) {
                    _.each(places.address_components, function(address_component) {
                        if (address_component) {
                            _.find(address_component.types, function(type) {
                                var keyFound;
                                var found = _.find(commonConfig.googleMapAddressInterest, function(googleMapAddressInterest, key) {
                                    if (type == googleMapAddressInterest) {
                                        keyFound = key;
                                        return true;
                                    }
                                });
                                if (keyFound) {
                                    var name = (commonConfig.googleMapAddressInterestIsShortName[keyFound] ? 'short' : 'long') + '_name';
                                    aryDetailResult[keyFound] = address_component[name];
                                    return true;
                                }
                                return false;
                            });
                        }
                    });
                }
            }
            if (aryDetailResult['city'] == '') {
                aryDetailResult['city'] = aryDetailResult['locality'];
            }

            return aryDetailResult;
        },
        getStringExtension: function(filename) {
            var regex = /(?:\.([^.]+))?$/;
            return regex.exec(filename)[1];
        },
        requestServerNotAPI: function() {
            return commonConfig.requestServer.replace('api/', '');
        }
    };

    //some array haven't implement find
    if (!Array.prototype.find) {
        Array.prototype.find = function(callback, thisArg) {
            "use strict";
            var arr = this,
                arrLen = arr.length,
                i;
            for (i = 0; i < arrLen; i += 1) {
                if (callback.call(thisArg, arr[i], i, arr)) {
                    return arr[i];
                }
            }
            return undefined;
        };
    }

    /* end Global variable */
});
