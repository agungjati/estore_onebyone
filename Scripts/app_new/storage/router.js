define(function(require, exports, module) {
    'use strict';

    const Backbone = require('backbone');
    require('backbone.subroute');

    var fnSetContentView = pathViewFile => {
        require([pathViewFile + '/view'], View => {
            if (View)
                commonFunction.setContentViewWithNewModuleView(new View());
        });
    };

    module.exports = Backbone.SubRoute.extend({
        initialize() {
            this.app = {};
        },
        routes: {
            'inward_goods(/*subrouter)': 'showInward_GoodsSubRouter',
        },
        showInward_GoodsSubRouter() {
            if (!this.app.inward_GoodsRouter) {
                require(['./inward_goods/router'], Router => {
                    this.app.inward_GoodsRouter = new Router(`${this.prefix}/inward_goods`, {
                        createTrailingSlashRoutes: true
                    })
                })
            }
        }
    });
});