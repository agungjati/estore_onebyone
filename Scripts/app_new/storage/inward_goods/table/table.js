// scripts/app/customers/list
define(function(require, exports, module) {
    'use strict';
    var Marionette = require('marionette');
    var template = require('text!./table.html');
    var Tbody = require('./tbody');

    module.exports = Marionette.View.extend({
        tagName: 'table',
        className: 'table table-unbordered table-hover',
        template: _.template(template),
        regions: {
            body: {
                el: 'tbody',
                replaceElement: true
            }
        },
        onRender: function() {
            this.showChildView('body', new Tbody({
                collection: this.collection
            }));
        }
    });
});
