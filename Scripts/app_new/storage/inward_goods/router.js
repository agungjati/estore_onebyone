//scripts/app/storage
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var commonFunction = require('commonfunction');
    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile, replaceMainContent) {
        var hashtag = '#storage/inward_goods';

        require(['./' + pathViewFile + '/view'], function(View) {
            var view = new View();

            if (replaceMainContent) {
                commonFunction.setContentViewWithNewModuleView(view, hashtag);
            } else {
            }
        });
    };

    module.exports = Backbone.SubRoute.extend({
        initialize: function() {
            this.app = {};
        },
        routes: {
            '': 'showList',
            'add' : 'showadd',
            'detail/:id': 'redirectToDetailModule',
            'detail/:id/edit': 'showEditModule',
        },
        showList(id, subrouter){
            fnSetContentView('', 'replace');
        },
        redirectToDetailModule: function(id, subrouter) {
            fnSetContentView('detail', 'replace');
        },
        showEditModule: function(){
            fnSetContentView('detail/edit', 'replace');
        },
        showadd(){
            fnSetContentView('add', 'replace');
        }
    });
});