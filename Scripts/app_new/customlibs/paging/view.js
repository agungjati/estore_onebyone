define(function(require, exports, module) {
    'use strict';

    var LayoutManager = require('layoutmanager');
    const template = require('text!pathCustomLibs/paging/template.html');

    module.exports = LayoutManager.extend({
        tagName: 'ul',
        className: 'pagination hide',
        template: _.template(template),
        initialize(){
            var self = this;
            this.listenTo(this.collection, 'sync', function(collection){
                this.$el.css('display','table').css('margin', '0 auto');
                this.$('[name="Page"]').val(collection.paramPaging.Page).attr('max', collection.paramPaging.TotalPage);
                this.$('[name="TotalPage"]').text(collection.paramPaging.TotalPage);

                self.changeButtonPaging();

            }, this);
        },
        events:{
            'click [obo-first]': 'goFirstPage',
            'click [obo-previous]': 'goPreviousPage',
            'click [obo-next]': 'goNextPage',
            'click [obo-last]': 'goLastPage',
            'keypress [name="Page"]': 'goToPageByKeyPress',
            'focusout [name="Page"]': 'goToPage',
        },
        beforeRender(){
            this.setIsHide();
        },
        afterRender(){
            var self = this;

            ['first', 'previous', 'next', 'last'].forEach(function(item){
                self.$('[obo-'+item + '] > img').attr('src','/Scripts/img/pagination/' + item + '_off.png');
            });
        },
        goFirstPage(){
            var page = 1;
            this.setChangePage(page);
        },
        goPreviousPage(){
            var page = this.$('[name="Page"]').val();

            if (--page <= this.collection.paramPaging.TotalPage && page >= 1){
                this.setChangePage(page);
            }
        },
        goNextPage(){
            var page = this.$('[name="Page"]').val();
            if (++page <= this.collection.paramPaging.TotalPage ){
                this.setChangePage(page);
            }
        },
        goLastPage(){
            var page = this.collection.paramPaging.TotalPage;
            this.setChangePage(page);
        },
        goToPageByKeyPress(e){
            if (e.charCode == 13){
                this.goToPage();
            }
        },
        goToPage(e){
            this.setChangePage(this.$('[name="Page"]').val());
        },
        changeButtonPaging(){
            var self = this;
            var isPageOne = this.collection.paramPaging.Page == 1;
            var isLastPage = this.collection.paramPaging.Page == this.collection.paramPaging.TotalPage;

            var fnChangeButtonPaging = function(arrayButtonSelector, isTrue ){
                _.each(self.$(arrayButtonSelector), function(item){
                    if (isTrue){
                        $(item).attr('disabled', 'disabled');
                    }else{
                        $(item).removeAttr('disabled');
                    }
                });
            }

            fnChangeButtonPaging('[obo-first], [obo-previous]', isPageOne);
            fnChangeButtonPaging('[obo-next], [obo-last]', isLastPage);
            this.setIsHide();
        },
        setChangePage(page){
            if (page >= 1 && page <= this.collection.paramPaging.TotalPage){
                this.collection.paramPaging.Page = page;
                this.$('[name="Page"]').val(page)
                this.collection.fetch();
                this.changeButtonPaging();
            }
        },
        setIsHide(){
            if (this.collection.paramPaging.Page > 1 || this.collection.length){
                this.$el.removeClass('hide');
            }else{
                this.$el.addClass('hide')
            }
        }
    })
});
