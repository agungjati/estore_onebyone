define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var commonFunction = require('commonfunction');
    require('backbone.subroute');
    let Radio = require('backbone.radio')

    var fnSetContentView = function(pathViewFile) {
        require([pathViewFile + '/view'], function(View) {
            if (View)
                commonFunction.setContentViewWithNewModuleView(new View());
        });
    };

    module.exports = Backbone.SubRoute.extend({
        initialize: function() {
            this.app = {};
            this.channelMasterData = Radio.channel('master_data')
            this.channelMasterData.reply('showAdd', this.showAdd)
        },
        routes: {
            '': 'showList',
            'add': 'showAdd',
            ':id/(*subrouter)': 'redirectToDetailModule'
        },
        showList() {
            fnSetContentView('.');
        },
        showAdd(){
            fnSetContentView('./add');
        },
        redirectToDetailModule: function(id, subrouter) {
            var self = this;
            require(['./detail/router'], function(Router) {
                if (!self.app.detailRouter) {
                    self.app.detailRouter = new Router(self.prefix + '/:id', {
                        createTrailingSlashRoutes: true
                    });
                }
            });
        }
    });
});
