define(function(require, exports, module) {
    'use strict';

    const Backbone = require('backbone'),
        Model = require('./model'),
        commonFunction = require('commonfunction')
    let eventAggregator = require('eventaggregator');

    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile, replaceMainContent, options) {
        var hashtag = '#customers';

        require(['./' + pathViewFile + '/view'], function(View) {
            let view = new View();
            let previousContentView = commonFunction.getContentView();
            let currentContentView = previousContentView && commonFunction.getContentView().getView('');

            if (replaceMainContent) {
                commonFunction.setContentViewWithNewModuleView(view, hashtag);
            } else {
                let name = previousContentView && previousContentView.getView('') && previousContentView.getView('').name;

                if (name != 'customers.detail') {
                    let MainView = require('./view');
                    let mainView = new MainView();
                    commonFunction.setContentViewWithNewModuleView(mainView, hashtag);
                    currentContentView = mainView;
                }
                currentContentView.setView('#myTabContent > .tab-pane.active', view);
                view.render();
            }
            if (options) {
                let { setActiveModule } = options
                if (setActiveModule) {
                    eventAggregator.trigger('master_data:customers:detail:menu:view:setActive')
                }
            }
        });
    };

    module.exports = Backbone.SubRoute.extend({
        initialize() {
            this.model = new Model();
            this.stopListening(eventAggregator, 'getCustomerModel');
            this.listenTo(eventAggregator, 'getCustomerModel', function(callback) {
                var id = commonFunction.getUrlHashSplit(3);
                if (!this.model.id || (this.model.id != id)) {
                    this.model.set(this.model.idAttribute, id);
                    this.model.once('sync', function(model) {
                        callback(model);
                    });
                    this.model.fetch();
                } else {
                    callback(this.model);
                }
            });

            this.listenTo(eventAggregator, 'master_data:customers:detail:view:changeContent', fnSetContentView)
            let Radio = require('backbone.radio')
            this.channelMasterData = Radio.channel('master_data')

        },
        routes: {
            '': 'redirectToCustomerProfile',
            'customer_profile': 'showCustomerProfile',
            'customer_profile/edit': 'showCustomerProfileEdit',
            'products': 'showProducts',
            'products/detail/:id': 'showProductsDetail',
            'addresses': 'showAddresses',
            'addresses/add': 'showAddressesAdd',
            'addresses/detail/:id': 'showAddressesDetail',
            'addresses/detail/:id/edit': 'showAddressesDetailEdit',
            'contacts': 'showContacts',
            'contacts/add': 'showContactsAdd',
            'contacts/detail/:id': 'showContactsDetail',
            'contacts/detail/:id/edit': 'showContactsDetailEdit',
            'senders_and_receivers': 'showSenderAndReceiver',
            'senders_and_receivers/add': 'showSenderAndReceiverAdd',
            'senders_and_receivers/detail/:id': 'showSenderAndReceiverDetail',
            'senders_and_receivers/detail/:id/edit': 'showSenderAndReceiverDetailEdit'
        },
        redirectToCustomerProfile() {
            if (window.location.hash.split('/')[2] == 'add'){
                this.channelMasterData.request('showAdd')
            }else{
                this.navigateBackbonePrototype(commonFunction.getCurrentHashToLevel(3).replace('#', '') + '/customer_profile', { trigger: true, replace: true });
            }
        },
        showAdd() {
            fnSetContentView('./../add');
        },
        showCustomerProfile() {
            fnSetContentView('customer_profile', undefined, {
                setActiveModule: true
            });
        },
        showCustomerProfileEdit() {
            fnSetContentView('customer_profile/edit', 'replace');
        },
        showProducts() {
            fnSetContentView('products', undefined, {
                setActiveModule: true
            });
        },
        showProductsDetail() {
            fnSetContentView('products/detail', 'replace');
        },
        showAddresses() {
            fnSetContentView('addresses', undefined, {
                setActiveModule: true
            });
        },
        showAddressesAdd() {
            fnSetContentView('addresses/add', 'replace');
        },
        showAddressesDetail() {
            fnSetContentView('addresses/detail', 'replace');
        },
        showAddressesDetailEdit() {
            fnSetContentView('addresses/detail/edit', 'replace');
        },
        showContacts() {
            fnSetContentView('contacts');
        },
        showContactsAdd() {
            fnSetContentView('contacts/add', 'replace');
        },
        showContactsDetail() {
            fnSetContentView('contacts/view', 'replace');
        },
        showContactsDetailEdit() {
            fnSetContentView('contacts/edit', 'replace');
        },
        showSenderAndReceiver() {
            fnSetContentView('senders_and_receivers');
        },
        showSenderAndReceiverAdd() {
            fnSetContentView('senders_and_receivers/add', 'replace');
        },
        showSenderAndReceiverDetail() {
            fnSetContentView('senders_and_receivers/detail', 'replace');
        },
        showSenderAndReceiverDetailEdit() {
            fnSetContentView('senders_and_receivers/detail/edit', 'replace');
        }
    });
});