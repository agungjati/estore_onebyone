// scripts/app/customers/detail/senders_and_receivers/detail/edit
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    require('select2');
    var Model = require('./../../model');
    var commonConfig = require('commonconfig');
    var commonFunction = require('commonfunction');
    var ladda = require('ladda.jquery');
    require('bootstrap-validator');
    require('jquery-ui');

    module.exports = LayoutManager.extend({
        // className: 'container-fluid',
        className: 'jarviswidget jarviswidget-sortable',
        attributes: {
            'data-widget-colorbutton': false,
            'data-widget-editbutton': false,
            role: 'widget'
        },
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();

            this.model.url = 'Customer/' + commonFunction.getUrlHashSplit(3) + '/SenderReceiver/' + commonFunction.getUrlHashSplit(6);
            this.model.set(this.model.idAttribute, commonFunction.getUrlHashSplit(6));

            this.listenToOnce(this.model, 'sync', function(model) {
                this.render();
                var data = model.toJSON();

                commonFunction.setSelect2GeneralCountry(this).then(function() {
                    self.$('[name="Country"]').val(data.CountryID).trigger("change");
                });

                commonFunction.setSelect2Status(this, {
                    selector: '[name="IsActive"]'
                }).then(function(object) {
                    var found = data.IsActive;
                    self.$(object.options.selector).val(found.toString()).trigger("change");
                });


                this.listenTo(this.model, 'sync', function() {
                    Backbone.history.navigate(commonFunction.getCurrentHashOmitSuffix(1), true);
                })
            }, this);

            this.once('afterRender', function() {
                this.model.fetch();
            });

            this.on('cleanup', function() {
                this.laddaDestroy();
            })
        },
        events: {
            'focus [name="Address1"]': 'getAutocompleteGooglePlace1',
            'focusout [name="Address1"]': 'checkAutoComplete1',
            'keydown [name="Address1"]': 'removeWarningMessage1'
        },
        afterRender: function() {
            if (!this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            }
            this.renderValidation();
        },
        renderValidation: function() {
            var self = this;

            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        PhoneNumber: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Phone Number is invalid'
                                }
                            }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        removeWarningMessage1: function() {
            this.$('[obo-warningMessage1]').remove();
        },
        checkAutoComplete1: function() {
            var self = this;

            var dom = this.$('[name="Address1"]');
            this.removeWarningMessage1();

            var found = _.find($('.ui-autocomplete li'), function(item) {
                return dom.val() == item.textContent;
            });
            if (!found) {
                dom.val('');
                this.$('[obo-dosave]').prop("disabled", true);
                var html = $('<small class="text-warning" style="display: inline;" obo-warningMessage1>Address Not Found</small>')
                this.$('[obo-view="error1"]').append(html);
                this.setAddresses();
            } else {
                self.removeWarningMessage1();
                self.$('[obo-dosave]').prop("disabled", false);
            }
        },
        getAutocompleteGooglePlace1: function() {
            var self = this;

            this.$('[name="Address1"]').autocomplete({
                source: function(req, resp) {
                    commonFunction.getGoogleMapAutoCompleteService().getPlacePredictions({
                        input: req.term,
                        types: ['geocode'],
                        componentRestrictions: {
                            country: 'au'
                        }
                    }, function(places, status) {
                        if (status === google.maps.places.PlacesServiceStatus.OK) {
                            var _places = [];
                            for (var i = 0; i < places.length; ++i) {
                                _places.push({
                                    id: places[i].place_id,
                                    value: places[i].structured_formatting.main_text,
                                    label: places[i].structured_formatting.main_text
                                });
                            }
                            self.$('[obo-dosave]').prop("disabled", false);
                            resp(_places);
                        } else {
                            $('.ui-autocomplete').css('display', 'none');
                            var html = $('<small class="text-warning" style="display: inline;" obo-warningMessage1>Address Not Found</small>')
                            self.$('[obo-view="error1"]').append(html);
                            self.setAddresses();
                            self.$('[obo-dosave]').prop("disabled", true);
                        }
                    });
                },
                select: function(event, object) {
                    if (object && object.item && object.item.id) {
                        commonFunction.getGoogleMapPlacesPlaceService().getDetails({
                            placeId: object.item.id
                        }, function(places, status) {
                            self.setAddresses(commonFunction.getGoogleMapDetailResult(places, status));
                        });
                    } else {
                        self.setAddresses();
                    }
                }
            });
        },
        setAddresses: function(results) {
            this.$('input[name="Postcode"]').val((results && results.postcode) || '');
            this.$('input[name="State"]').val((results && results.state) || '');
            this.$('input[name="City"]').val((results && results.city) || '');
        },
        doSave: function(e) {
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            data.CustomerID = commonFunction.getUrlHashSplit(3);
            data.Country = this.$('[name="Country"]').select2('data')[0].text;

            this.$('input[disabled]').each(function() {
                data[this.name] = $(this).val();
            });
            this.model.save(data);
        },
        laddaDestroy: function() {
            if (this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'].remove();
                delete this.ladda['obo-dosave'];
            }
        }
    });
});
