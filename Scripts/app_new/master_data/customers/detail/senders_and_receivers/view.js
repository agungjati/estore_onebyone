// scripts/app/customers/sendersandreceivers
define(function(require, exports, module) {
    'use strict';
    var View = require('./../commons/view');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Paging = require('paging');
    require('bootstrap-validator');
    var Filter = require('./filter/view');

    module.exports = View.extend({
        template: _.template(template),
        initialize: function() {
            this.table = new Table({
                collection: new Collection()
            });

            this.table.collection.url = 'Customer/' + commonFunction.getUrlHashSplit(3) + '/SenderReceiver';

            this.filter = new Filter({
                collection: this.table.collection
            });
            this.paging = new Paging({
                collection: this.table.collection
            });

            this.on('cleanup', function() {
                this.table.destroy();
                this.modalDialog && this.modalDialog.remove && this.modalDialog.remove();
            }, this)
        },
        afterRender: function() {
            this.insertView('[obo-filter]', this.filter);
            this.filter.render();

            this.$('[obo-table]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.table.collection.fetch();
        },
        showEditFilters: function() {
            var self = this;
            require(['./modaldialogeditfilter/view'], function(ModalDialog) {
                self.showModalDialog(ModalDialog);
            });
        }
    });
});
