define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'SenderAndReceiverID',
        defaults: function() {
            return {
              Address1: '',
              Address2: '',
              FirstName: '',
              LastName: '',
              PhoneNumber: '',
              Email: '',
              Country: '',
              CountryID :'',
              State: '',
              City: '',
              Postcode: '',
              InsertDate: '',
              InsertBy: '',
              UpdateDate: '',
              UpdateBy: '',
              CustomerID: '',
              AccountCode :'',
              IsActive : ''
            }
        }
    });
});
