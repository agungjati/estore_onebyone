// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');

    module.exports = Model.extend({
        urlRoot: 'Customer/address',
        defaults: function() {
            return {
                AddressType: '',
                Address1: '',
                Address2: '',
                Country: '',
                Postcode: '',
                City: '',
                State: '',
                FirstName: '',
                LastName: '',
                PhoneNumber: '',
                FaxNumber: '',
                Email: '',
            }
        }
    });
});
