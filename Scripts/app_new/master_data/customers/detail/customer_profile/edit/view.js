// scripts/app/customers/detail/customer_profile/edit
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var Model = require('./../../../model');
    var ladda = require('ladda.jquery');
    var Cookies = require('Cookies');
    require('bootstrap-validator');
    require('select2');
    require('datetimepicker');
    require('tagsinput');

    module.exports = LayoutManager.extend({
        // className: 'container-fluid',
        className: 'jarviswidget jarviswidget-sortable',
        attributes: {
            'data-widget-colorbutton': false,
            'data-widget-editbutton': false,
            role: 'widget'
        },
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();

            this.model.set(this.model.idAttribute, commonFunction.getUrlHashSplit(3));

            this.listenToOnce(this.model, 'sync', function(model) {
                this.render();
                var data = model.toJSON();
                commonFunction.setSelect2CustomerAccountOwner(this, {
                    selector: '[name="AccountOwnerID"]'
                }).then(function(object) {
                    self.$(object.options.selector).val(data.AccountOwner.AccountOwnerID).trigger('change');
                });

                commonFunction.setSelect2CustomerPaymentTerm(this).then(function(object) {
                    if (data.PaymentTerm) {
                        self.$(object.options.selector).val(data.PaymentTerm.PaymentTermID).trigger('change');
                    }
                });

                commonFunction.setSelect2CustomerFreightPayable(this).then(function(object) {
                    if (data.FreightPayable) {
                        self.$(object.options.selector).val(data.FreightPayable.FreightPayableID).trigger('change');
                    }
                });

                commonFunction.setSelect2GeneralCurrencies(this).then(function(object) {
                    self.$(object.options.selector).val(data.Currency).trigger('change');
                });

                commonFunction.setSelect2Status(this, {
                    selector: '[name="IsActive"]'
                }).then(function(object) {
                    var isActive = data.IsActive;
                    self.$(object.options.selector).val(isActive.toString()).trigger("change");
                });

                this.listenTo(this.model, 'sync', function() {
                    Backbone.history.navigate(commonFunction.getCurrentHashOmitSuffix(1), true);
                })
            }, this);


            // commonFunction.setSelect2CustomerTags(this);

            this.once('afterRender', function() {
                this.model.fetch();
            });

            this.listenTo(this.model, 'request', function() {
                self.$('[obo-dosave]').attr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('start');

            });

            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('stop');
            });

            var selector = this.$('[name="IsActive"]');
            this.on('beforeRender beforeRemove cleanup', function() {
                if (selector.length && selector.hasClass('select2-hidden-accessible')) {
                    selector.select2('destroy');
                }
            });

            this.on('afterRender', function() {
                if (selector.length && !selector.hasClass('select2-hidden-accessible')) {
                    selector.select2();
                }

                selector.val(this.model.get('IsActive').toString()).trigger('change');
            });

            this.on('cleanup', function(){

            })
        },
        events: {
            'click [obo-addtags]': 'addTags'
        },
        afterRender: function() {
            var self = this;
            this.laddaDestroy();
            this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();

            this.$('[name="DateStart"]').datetimepicker({
                defaultDate: new Date(),
                format: commonConfig.datePickerFormat
            });
            var authorization = Cookies.get(commonConfig.cookieFields.Authorization);

            this.$('[name="TagsValue"]').select2({
                minimumInputLength: 1,
                tags: [],
                ajax: {
                    headers: {
                        'Authorization': authorization
                    },
                    url: commonConfig.requestServer + 'Customer/Tags',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function(params) {
                        var queryParameters = {
                            DisplayRows: 100,
                            Page: 1,
                            OrderBy: '',
                            TagName: params.term
                        }
                        return queryParameters;
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.ResultList, function(item) {
                                return {
                                    text: item.TagName,
                                    id: item.TagID
                                }
                            })
                        };
                    }
                }
            });
            this.$('[name="TagsValue"]').addClass('hidden')
            this.$('[name="TagName"]').tagsinput({
                itemValue: 'value',
                itemText: 'text',
                tagClass: 'label label-default'
            });

            _.each(this.model.get('ListCustomerTag'), function(item) {
                self.$('[name="TagName"]').tagsinput('add', {
                    "value": item.TagID,
                    "text": item.TagName
                });
            });

            this.renderValidation();
        },
        renderValidation: function() {
            var self = this;

            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        CustomerABN: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{11,11}$/
                                }
                            }
                        },
                        CustomerACN: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{9,9}$/
                                }
                            }
                        },
                        Phone: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Phone Number is invalid'
                                }
                            }
                        },
                        Mobile: {
                            validators: {
                                regexp: {
                                    regexp: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/,
                                    message: 'The format of Mobile Number is invalid'
                                }
                            }
                        },
                        Fax: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Fax Number is invalid'
                                }
                            }
                        }
                    }
                })
                .off('success.form.bv')
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        addTags: function() {
            var data = this.$('[name="TagsValue"]').select2('data');
            var dataObject = {};
            if (!(data && data[0] && data[0].text)) {
                return;
            }

            dataObject.value = parseInt(data[0].id) || data[0].text;
            dataObject.text = data[0].text;

            this.$('[name="TagName"]').tagsinput('add', dataObject);
        },
        doSave: function(e) {
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            var tagName = [];
            var tmpTagName = this.$('[name="TagName"]').tagsinput('items');

            tmpTagName.forEach(function(item) {
                tagName.push(item.text);
            });
            data.PaymentTermID = this.$('[name="PaymentTermID"]').val();
            data.FreightPayableID = this.$('[name="FreightPayableID"]').val();
            data.DateStart = $('[name="DateStart"]').data('DateTimePicker').date().format('YYYYMMDD');
            data.TagName = tagName;
            this.model.save(data);
        },
        laddaDestroy: function(){
            if (this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'].remove();
                delete this.ladda['obo-dosave'];
            }
        }
    });
});
