// scripts/app/customers/sendersandreceivers
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');

    module.exports = LayoutManager.extend({
        className: 'container-fluid',
        events: {
            'click [obo-defaultfilter]': 'setFilterToDefault',
            'click [obo-showeditfilters]': 'showEditFilters'
        },
        setFilterToDefault: function() {
            this.filter && this.filter.setFilterToDefault();
        },
        showEditFilters: function() {
            var self = this;
            require(['./modaldialogeditfilter/view'], function(ModalDialog) {
                self.showModalDialog(ModalDialog);
            });
        },
        showModalDialog: function(ModalDialog){
            var self = this;
            this.modalDialog = new ModalDialog({
                viewFilter: this.filter
            });

            $('body').append(this.modalDialog.el);
            this.modalDialog.$el.on('hidden.bs.modal', function() {
                self.modalDialog.remove();
            });

            this.modalDialog.once('afterRender', function() {
                self.modalDialog.$el.modal();
            });
            this.modalDialog.render();
        }
    });
});
