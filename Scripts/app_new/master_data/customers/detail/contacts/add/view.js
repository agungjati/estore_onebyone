// scripts/app/customers/detail/contacts/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Model = require('./model');
    var ladda = require('ladda.jquery');
    var eventAggregator = require('eventaggregator');
    require('select2');
    require('bootstrap-validator');

    module.exports = LayoutManager.extend({
        // className: 'container-fluid',
        className: 'jarviswidget jarviswidget-sortable',
        attributes: {
            'data-widget-colorbutton': false,
            'data-widget-editbutton': false,
            role: 'widget'
        },
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();

            this.listenTo(this.model, 'request', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled').html('SAVE');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled').html('SAVE');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.listenTo(this.model, 'sync', function(model) {
                Backbone.history.navigate(commonFunction.getCurrentHashOmitSuffix(1), true);
            });

            this.listenTo(eventAggregator, 'customers/detail/contacts/add/addAnotherSocialMedia', function() {
                var sosMedViews = this.getViews('[obo-view="addAnotherSocialMedia"]').value();
                if (sosMedViews.length != 5) {
                    this.addAnotherSocialMedia();
                }
            }, this);

            this.listenTo(eventAggregator, 'customers/detail/contacts/add/setButtonAddSocialMedia', function() {
                var sosMedViews = this.getViews('[obo-view="addAnotherSocialMedia"]').value();
                if (sosMedViews.length != 5) {
                    _.last(sosMedViews).changeButtonToAdd();
                    _.first(sosMedViews).showTitle();
                }

            }, this);

            commonFunction.setSelect2ContactType(this);

            this.on('cleanup', function() {
                this.laddaDestroy();
            });
        },
        afterRender: function() {
            if (!this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            }

            this.addAnotherSocialMedia();

            this.renderValidation();
        },
        renderValidation: function() {
            var self = this;

            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        PhoneNumber: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Phone Number is invalid'
                                }
                            }
                        },
                        MobileNumber: {
                            validators: {
                                regexp: {
                                    regexp: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/,
                                    message: 'The format of Mobile Number is invalid'
                                }
                            }
                        },
                        FaxNumber: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Fax Number is invalid'
                                }
                            }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        doSave: function() {
            var data = {};
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            data.CustomerID = commonFunction.getUrlHashSplit(3);
            data.SocialMedia = [];

            eventAggregator.trigger('customers/detail/contacts/add/view', function(obj) {
                if (obj){
                    if (obj.SocialMediaID && obj.SocialMediaUrl){
                        data.SocialMedia.push(obj)
                    }else if (obj.SocialMediaUrl){
                    }
                } else {
                }
            });

            this.model.save(data);
            this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            this.ladda['obo-dosave'].ladda('start');
        },
        addAnotherSocialMedia: function(options) {
            var self = this;
            options = options || {};
            require(['./addAnotherSocialMedia/view'], function(View) {

                var length = self.getViews('[obo-view="addAnotherSocialMedia"]').value().length;
                if (!length){
                    options.showLabel = true;
                }

                var view = new View(options);
                self.insertView('[obo-view="addAnotherSocialMedia"]', view);
                view.render();



            });
        },
        laddaDestroy: function() {
            if (this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'].remove();
                delete this.ladda['obo-dosave'];
            }
        }
    });
});
