// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');

    module.exports = Model.extend({
        urlRoot: 'Customer/Contacts',
        defaults: function() {
            return {
                FirstName: '',
                LastName: '',
                PhoneNumber: '',
                FaxNumber: '',
                Email: '',
                Website: '',
                SocialMedia: ''
            }
        }
    });
});
