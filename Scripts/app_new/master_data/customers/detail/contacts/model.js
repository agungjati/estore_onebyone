define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');
    var commonFunction = require('commonfunction');

    module.exports = Model.extend({
        idAttribute: 'CustomerID',
        defaults: function() {
            return {
              ContactType:{
                ContactTypeName: ''
              },
              FirstName: '',
              LastName: '',
              PhoneNumber: '',
              FaxNumber: '',
              MobileNumber:'',
              Email: '',
              Website: '',
              IsActive: '',
              SocialMedias: []
            }
        }
    });
});
