// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var Model = require('./model');
    var Cookies = require('Cookies');
    var ladda = require('ladda.jquery');
    var eventAggregator = require('eventaggregator');
    require('select2');
    require('datetimepicker');
    require('moment');
    require('tagsinput');
    require('maxlength');
    require('bootstrap-validator');
    var UrlRoot = commonConfig.requestServer;
    var authorization = Cookies.get(commonConfig.cookieFields.Authorization);

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();
            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled').html('Save');
                self.ladda['obo-dosave'].ladda('stop');
            });
            this.listenTo(this.model, 'request', function() {
                // self.$('button[type=submit]').ladda('bind', {
                //     timeout: 2000
                // });
                self.ladda['obo-dosave'].ladda('start');
            });
            this.listenTo(this.model, 'sync', function(model) {
                Backbone.history.navigate('products', true);
            });
            commonFunction.setSelect2ProductSKUType(this, {
                selector: '[name="SKUTypeID"]',
                placeholder: 'Select SKU Type'
            });
            commonFunction.setSelect2ProductProductCategory(this, {
                selector: '[name="ProductCategoryID"]'
            });
            commonFunction.setSelect2ProductManufactureLeadTime(this);
            commonFunction.setSelect2StorageChargePer(this)
        },
        events: {
            // 'click [obo-dosave]': 'doSave',
            'change [name="IsDangerousGoods"]': 'changeRadioDangerousGoods',
            'change [name="IsTemperatureSensitive"]': 'changeTemperatureSensitive',
            'change [name="SKUTypeID"]': 'changeSKUType',
            'change [name="Length"]':'calculateCubic',
            'change [name="Width"]':'calculateCubic',
            'change [name="Height"]':'calculateCubic',
            'change [name="TemperatureValueMin"]':'changeTemperatureValueMin'
        },
        afterRender: function() {
          if (!this.ladda['obo-dosave']) {
              this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
          }
            $('[name="ProductCode"]').maxlength({
                max: 50,
                feedbackText: 'Text limited to {m} characters. Remaining: {r}'
            });
            $('[name="ProductDescription"]').maxlength({
                max: 500,
                feedbackText: 'Character limited to {m} characters. Remaining: {r}'
            });
            this.$('[date]').datetimepicker({
                defaultDate: new Date(),
                format: commonConfig.datePickerFormat,
            });
            var authorization = Cookies.get(commonConfig.cookieFields.Authorization);
            this.$('[name="CustomerID"]').select2({
                minimumInputLength: 1,
                ajax: {
                    headers: {
                        'Authorization': authorization
                    },
                    url: commonConfig.requestServer + 'Customer',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function(params) {
                        var queryParameters = {
                            DisplayRows: 100,
                            Page: 1,
                            OrderBy: '',
                            FilterParams: [{
                                Key: 'CustomerName',
                                Value: params.term
                            }]
                        }
                        return queryParameters;
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.ResultList, function(item) {
                                return {
                                    text: item.CustomerName,
                                    id: item.CustomerID
                                }
                            })
                        };
                    }
                }
            });
            this.renderValidation();
        },
        changeTemperatureValueMin : function(){
            var max = this.$('[name="TemperatureValueMax"]').val();
            var min = this.$('[name="TemperatureValueMin"]').val();
            if(min > max){
                this.$('[name="TemperatureValueMin"]').val('');
                this.$('[name="TemperatureValueMin"]').attr('placeholder', "input must be smaller  than "+max);
            }
        },
        renderValidation: function() {
            var self = this;

            this.$('[obo-form]').bootstrapValidator()
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        calculateCubic : function(){
          var l = this.$('[name="Length"]').val();
          var w = this.$('[name="Width"]').val();
          var h = this.$('[name="Height"]').val();
          this.$('[name="CubicMetres"]').val(l*w*h);
        },
        changeSKUType: function() {
            var value = this.$('[name="SKUTypeID"]').select2('data')[0].text;
            this.renderProductpactUnit(value);
            this.renderStorageChargeUnit(value);
            this.renderTransportChargeUnit(value);
            var attribute = this.$('[name="ItemQuantity"], [name="ItemPerInnerQuantity"], [name="ItemPerCartonQuantity"], [name="QuantityPalletPerCarton"]');
            var attributemute = this.$('[obo-view="ItemQuantity"] label, [obo-view="ItemPerInnerQuantity"] label, [obo-view="ItemPerCartonQuantity"] label, [obo-view="QuantityPalletPerCarton"] label');
            this.$(attributemute).addClass('text-muted');
            attribute.prop('disabled', true);
            switch (value) {
                case 'Carton':
                var attribute = this.$('[name="ItemPerCartonQuantity"],[name="QuantityPalletPerCarton"]');
                    var attributeMuted = this.$('[obo-view="ItemPerCartonQuantity"] label,[obo-view="QuantityPalletPerCarton"] label');
                    this.$(attributeMuted).removeClass('text-muted');
                    attribute.prop('disabled', false);
                    break;
                case 'Inner':
                var attribute = this.$('[name="ItemPerInnerQuantity"],[name="ItemPerCartonQuantity"],[name="QuantityPalletPerCarton"]');
                    var attributeMuted = this.$('[obo-view="ItemPerInnerQuantity"] label,[obo-view="ItemPerCartonQuantity"] label,[obo-view="QuantityPalletPerCarton"] label');
                    this.$(attributeMuted).removeClass('text-muted');
                    attribute.prop('disabled', false);
                    break;
                case 'Item':
                var attribute = this.$('[name="ItemQuantity"],[name="ItemPerInnerQuantity"],[name="ItemPerCartonQuantity"],[name="QuantityPalletPerCarton"]');
                    var attributeMuted = this.$('[obo-view="ItemQuantity"] label,[obo-view="ItemPerInnerQuantity"] label,[obo-view="ItemPerCartonQuantity"] label,[obo-view="QuantityPalletPerCarton"] label');
                    this.$(attributeMuted).removeClass('text-muted');
                    attribute.prop('disabled', false);
                    break;
                case 'Pallet':
                var attribute = this.$('[name="QuantityPalletPerCarton"]');
                    var attributeMuted = this.$('[obo-view="QuantityPalletPerCarton"] label');
                    this.$(attributeMuted).removeClass('text-muted');
                    attribute.prop('disabled', false);
                    break;
                default:

            }

            // if(value == 'Item'){
            //   this.$('[obo-view] label').removeClass('text-muted');
            //   attribute.prop('disabled', false);
            // }
        },
        renderProductpactUnit: function(value) {
            var self = this;
            var authorization = Cookies.get(commonConfig.cookieFields.Authorization);

            $.ajax({
                url: UrlRoot + "Product/ProductPackUnit?SKUType=" + value,
                headers: {
                    'Authorization': authorization
                },
                success: function(data) {
                    var option = new Option('Select Product Pack Unit');
                    $(option).attr('selected', 'selected').attr('disabled', 'disabled');
                    self.$('[name="ProductPackUnit"]').empty().append(option);
                    _.each(data, function(item) {
                        self.$('[name="ProductPackUnit"]').append(new Option(item.ProductPackUnitName, item.ProductPackUnitID));
                    });
                    self.$('[name="ProductPackUnit"]').prop('disabled', false);
                    self.$('[name="ProductPackUnit"]').select2();
                }
            });
        },
        renderStorageChargeUnit: function(value) {
            var self = this;
            var authorization = Cookies.get(commonConfig.cookieFields.Authorization);

            $.ajax({
                url: UrlRoot + "Product/StorageChargeUnit?SKUType=" + value,
                headers: {
                    'Authorization': authorization
                },
                success: function(data) {
                    var option = new Option('Select Storage Charge Unit');
                    $(option).attr('selected', 'selected').attr('disabled', 'disabled');
                    self.$('[name="StorageChargeUnit"]').empty().append(option);
                    _.each(data, function(item) {
                        self.$('[name="StorageChargeUnit"]').append(new Option(item.StorageChargeUnitName, item.StorageChargeUnitID));
                    });
                    self.$('[name="StorageChargeUnit"]').prop('disabled', false);
                    self.$('[name="StorageChargeUnit"]').select2();
                }
            });
        },
        renderTransportChargeUnit: function(value) {
            var self = this;
            var authorization = Cookies.get(commonConfig.cookieFields.Authorization);

            $.ajax({
                url: UrlRoot + "Product/TransportChargeUnit?SKUType=" + value,
                headers: {
                    'Authorization': authorization
                },
                success: function(data) {
                    var option = new Option('Select Transport Charge Unit');
                    $(option).attr('selected', 'selected').attr('disabled', 'disabled');
                    self.$('[name="TransportChargeUnit"]').empty().append(option);
                    _.each(data, function(item) {
                        self.$('[name="TransportChargeUnit"]').append(new Option(item.TransportChargeUnitName, item.TransportChargeUnitID));
                    });
                    self.$('[name="TransportChargeUnit"]').prop('disabled', false);
                    self.$('[name="TransportChargeUnit"]').select2();
                }
            });
        },
        changeRadioDangerousGoods: function() {
            var self = this;
            var value = this.$('[name="IsDangerousGoods"]:checked').val();
            if (value == 'true') {
                require(['./isYesDangerousGoods/view'], function(View) {
                    var view = new View();
                    self.removeView('[obo-view="IsYesDangerousGoods"]');
                    self.insertView('[obo-view="IsYesDangerousGoods"]', view);
                    view.render();
                });
            } else {
                self.removeView('[obo-view="IsYesDangerousGoods"]');
            }
        },
        changeTemperatureSensitive: function() {
            var self = this;
            var value = this.$('[name="IsTemperatureSensitive"]:checked').val();
            if (value == 'true') {
                require(['./isYesTemperatureSensitive/view'], function(View) {
                    var view = new View();
                    self.removeView('[obo-view="IsYesTemperatureSensitive"]');
                    self.insertView('[obo-view="IsYesTemperatureSensitive"]', view);
                    view.render();
                });
            } else {
                self.removeView('[obo-view="IsYesTemperatureSensitive"]');
            }
        },
        doSave: function(e) {
            var data = {};
            if (e && e.currentTarget) {
                $(e.currentTarget).attr('disabled', 'disabled').html('Loading...');
            }
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());

            eventAggregator.trigger('products/add/view', function(obj) {
                $.extend(data, obj);
            });
            if(this.$('[name="file"]').length != 0){
              data.Attachment = this.$('[name="file"]')[0].files[0];
            }
            data.IsDangerousGoods = this.$('[name="IsDangerousGoods"]:checked').val();
            data.IsTemperatureSensitive = this.$('[name="IsTemperatureSensitive"]:checked').val();

            this.model.save(data, {useThisUrl: commonFunction.requestServerNotAPI() + 'ProductLogistical/CreateProduct'});

            this.ladda['obo-dosave'] = this.$(e.currentTarget).ladda();
            this.ladda['obo-dosave'].ladda('start');

        }
    });
});
