// scripts/app/customers/add
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var eventAggregator = require('eventaggregator');

    module.exports = LayoutManager.extend({
        template: _.template(template),
        initialize: function() {

            debugger;
            this.listenTo(eventAggregator, 'products/add/view', function(fn) {
                var data = commonFunction.formDataToJson(this.$('form').serializeArray());
                fn({
                    'DangerousGoods': data
                });
            });
        },
        afterRender: function() {
            var self = this;
            commonFunction.setSelect2ProductSubRisk(this).then(function(object) {
                object.view.stopListening(object.collection, 'sync');
                if (self.model) {
                    var DangerousSubRisk = self.model.get('DangerousSubRisk');
                    if (DangerousSubRisk) {
                        object.view.$('[name="DangerousSubRisk"]').val(DangerousSubRisk.SubRiskID).trigger("change");
                    }
                }
            });
            commonFunction.setSelect2ProductUNNumber(this).then(function(object) {
                object.view.stopListening(object.collection, 'sync');
                if (self.model) {
                    var DangerousUNNumber = self.model.get('DangerousUNNumber');
                    if (DangerousUNNumber) {
                        object.view.$('[name="DangerousUNNumber"]').val(DangerousUNNumber.UNNumberID).trigger("change");
                    }
                }
            });
            commonFunction.setSelect2ProductHazchem(this).then(function(object) {
                object.view.stopListening(object.collection, 'sync');
                if (self.model) {
                    var DangerousHazchem = self.model.get('DangerousHazchem');
                    if (DangerousHazchem) {
                        object.view.$('[name="DangerousHazchem"]').val(DangerousHazchem.HazchemID).trigger("change");
                    }
                }
            });
            commonFunction.setSelect2ProductChemical(this).then(function(object) {
                object.view.stopListening(object.collection, 'sync');
                if (self.model) {
                    var DangerousChemicalName = self.model.get('DangerousChemicalName');
                    if (DangerousChemicalName) {
                        object.view.$('[name="DangerousChemicalName"]').val(DangerousChemicalName.ChemicalID).trigger("change");
                    }
                }
            });
            commonFunction.setSelect2ProductClassProduct(this).then(function(object) {
                object.view.stopListening(object.collection, 'sync');
                if (self.model) {
                    var ClassProduct = self.model.get('ClassProduct');
                    if (ClassProduct) {
                        object.view.$('[name="ClassProduct"]').val(ClassProduct.ClassProductID).trigger("change");
                    }
                }
            });;
        }
    });
});
