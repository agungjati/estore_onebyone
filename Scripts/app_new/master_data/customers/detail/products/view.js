// scripts/app/customers/detail/products
define(function(require, exports, module) {
    'use strict';
    var DefaultModule = require('defaultmodule');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Filter = require('./filter/view');
    var Paging = require('paging');
    var ModalDialog = require('./modaldialogeditfilter/view');
    require('select2');

    module.exports = DefaultModule.extend({
        // className: 'main-content',
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.table = new Table({
                collection: new Collection()
            });
            this.table.collection.url = 'Product/'+commonFunction.getUrlHashSplit(3) +'/Customer'
            this.filter = new Filter({
                collection: this.table.collection
            });

            this.paging = new Paging({
                collection: this.table.collection
            });

            this.ModalDialog = ModalDialog;

            this.on('cleanup', function() {
                this.table.remove();
            });
        },
        afterRender: function() {
            this.insertView('[obo-filter]', this.filter);
            this.filter.render();

            this.$('[obo-table]').append(this.table.el);

            this.table.render();

            this.table.collection.fetch();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.$('select').select2();
        }
    });
});
