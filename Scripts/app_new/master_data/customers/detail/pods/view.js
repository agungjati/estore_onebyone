// scripts/app/customers/detail/pods
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonConfig = require('commonconfig');
    require('datetimepicker');

    module.exports = LayoutManager.extend({
        template: _.template(template),
        events: {
          'click [obo-showedetail]': 'showedetail',
        },
        afterRender : function(){
          this.$('[date]').datetimepicker({
              // defaultDate: new Date(),
              format: commonConfig.datePickerFormat
          });
        },
        showedetail : function(){
          var self = this;
          require(['./modaldialogdetail/view'], function(ModalDialog) {
              var modalDialog = new ModalDialog();
              $('body').append(modalDialog.el);
              modalDialog.$el.on('hidden.bs.modal', function() {
                  modalDialog.remove();
              });

              modalDialog.once('afterRender', function() {
                  modalDialog.$el.modal();
              });
              modalDialog.render();
          });
        }
    });
});
