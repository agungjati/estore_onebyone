define(function(require, exports, module) {
    'use strict';
    
    var LayoutManager = require('layoutmanager'),
    	template = require('text!./template.html')

    let eventAggregator = require('eventaggregator')

    module.exports = LayoutManager.extend({
        tagName: 'ul',
        className: 'nav nav-tabs in',
        template: _.template(template),
        initialize(){
        	this.listenTo(eventAggregator,'master_data:customers:detail:menu:view:setActive', this.setActive)
        },
        events:{
        	'click a': 'linkModuleClicked'
        },
        afterRender(){
        	this.setActive()
        },
        linkModuleClicked(e) {
        	e.preventDefault()
        	let link = e.currentTarget.getAttribute('href')
        	if(link){
        		let linkChunks = link.split('/')
        		if (linkChunks.length == 4){
	        		const moduleName = linkChunks[linkChunks.length -1]
					eventAggregator.trigger('master_data:customers:detail:view:changeContent', moduleName)
	        		
	        		this.setActive({ e })
	        		window.history.replaceState({}, '', link)
	        	}
        	}
        },
        setActive(options = {}){
        	let { e } = options,
        	$Object = undefined

        	this.$('li').removeClass('active')
        	if(e && e.currentTarget){
        		$Object = $(e.currentTarget)
        	}else{
				const hash = window.location.hash
        		$Object = this.$(`[href="${hash}"]`)
        	}

        	if ($Object && $Object.length){
        		$Object.parent().addClass('active')
        	}
        }
    });
});