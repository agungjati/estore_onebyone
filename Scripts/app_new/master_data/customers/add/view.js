define(function(require, exports, module) {
    'use strict';

    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var Model = require('./model');
    var ladda = require('ladda.jquery');
    var Cookies = require('Cookies');
    require('select2');
    require('datetimepicker');
    require('tagsinput');
    require('bootstrap-validator');

    module.exports = LayoutManager.extend({
        // className: 'container-fluid main-content',
        className: 'jarviswidget jarviswidget-sortable',
        attributes: {
            'data-widget-colorbutton': false,
            'data-widget-editbutton': false,
            role: 'widget'
        },
        template: _.template(template),
        initialize: function() {
            var self = this;
            this.ladda = {};
            this.model = new Model();

            this.listenTo(this.model, 'request', function() {
                self.$('[obo-dosave]').attr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('start');
            });

            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.listenTo(this.model, 'sync', function(model) {
                let hastagSplit = window.location.hash.split('/')
                hastagSplit.pop()
                window.location.hash = hastagSplit.join('/')
                // Backbone.history.navigate('customers', true);
            });

            commonFunction.setSelect2CustomerAccountOwner(this, {
                selector: '[name="AccountOwnerID"]'
            });

            commonFunction.setSelect2CustomerPaymentTerm(this, {
                placeholder: 'Select Account Terms'
            });
            commonFunction.setSelect2GeneralCurrencies(this, {
                placeholder: 'Currency'
            });
            commonFunction.setSelect2CustomerFreightPayable(this);
        },
        events: {
            'click [obo-addtags]': 'addTags',
        },
        afterRender: function() {
            if (!this.ladda['obo-dosave']) {
                this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();
            }

            this.$('[name="DateStart"]').datetimepicker({
                defaultDate: new Date(),
                format: commonConfig.datePickerFormat
            });

            var authorization = Cookies.get(commonConfig.cookieFields.Authorization);
            this.$('[name="TagsValue"]').select2({
                minimumInputLength: 1,
                tags: [],
                width: '100%',
                ajax: {
                    headers: {
                        'Authorization': authorization
                    },
                    url: commonConfig.requestServer + 'Customer/Tags',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function(params) {
                        var queryParameters = {
                            DisplayRows: 100,
                            Page: 1,
                            OrderBy: '',
                            TagName: params.term
                        }
                        return queryParameters;
                    },
                    processResults: function(data) {
                        return {
                            results: $.map(data.ResultList, function(item) {
                                return {
                                    text: item.TagName,
                                    id: item.TagID
                                }
                            })
                        };
                    }
                }
            });
            this.$('[name="TagsValue"]').addClass('hidden')





           
            this.$('[name="TagName"]').tagsinput({
                tagClass: 'label label-default',
                itemValue: 'id',
                itemText: 'text'
            });

            this.renderValidation();
        },
        renderValidation: function() {
            var self = this;

            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        CustomerABN: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{11,11}$/
                                }
                            }
                        },
                        CustomerACN: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{9,9}$/
                                }
                            }
                        },
                        Phone: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Phone Number is invalid'
                                }
                            }
                        },
                        Mobile: {
                            validators: {
                                regexp: {
                                    regexp: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/,
                                    message: 'The format of Mobile Number is invalid'
                                }
                            }
                        },
                        Fax: {
                            validators: {
                                regexp: {
                                    regexp: /^(?=.*[1-9].*)[0-9]{5,10}$/,
                                    message: 'The format of Fax Number is invalid'
                                }
                            }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        addTags: function() {
            var data = this.$('[name="TagsValue"]').select2('data');
            var dataObject = {};
            if (data[0].text == '') {
                return;
            }

            ['id', 'text'].forEach(function(field) {
                dataObject[field] = data[0][field];
            });

            this.$('[name="TagName"]').tagsinput('add', dataObject);
        },
        doSave: function() {
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());
            var tagName = [];
            var tmpTagName = this.$('[name="TagName"]').tagsinput('items');

            tmpTagName.forEach(function(item) {
                tagName.push(item.text);
            });

            data.DateStart = $('[name="DateStart"]').data('DateTimePicker').date().format('YYYYMMDD');
            data.TagName = tagName;
            this.model.save(data);
        }
    });
});