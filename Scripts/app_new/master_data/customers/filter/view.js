// scripts/app/customers/filter
define(function(require, exports, module) {
    'use strict';
    
    var Filter = require('filter');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    require('datetimepicker');

    module.exports = Filter.extend({
        template: _.template(template),
        defaultShowFields: ['CustomerName', 'AccountNo', 'AccountName', 'TagName', 'IsActive'],
        initialize: function() {
            this.fieldsShowed = this.defaultShowFields.slice();

            commonFunction.setSelect2CustomerAccountOwner(this, {
                selector: '[name="AccountName"]',
                placeholder: 'Account Owner'
            });

            commonFunction.setSelect2CustomerTags(this, {
                selector: '[name="TagName"]',
                placeholder: 'Tag'
            });

            commonFunction.setSelect2CustomerPaymentTerm(this, {
                selector: '[name="PaymentTermName"]',
                placeholder: 'Account Terms'
            });

            commonFunction.setSelect2CustomerFreightPayable(this, {
                selector: '[name="FreightPayableName"]',
                placeholder : 'Feight Payable'
            });

            commonFunction.setSelect2Status(this, {
                selector: '[name="IsActive"]',
                placeholder: 'Status'
            });

            this.on('cleanup', function(){
                if (this.$('[name="DateStart"]').data('DateTimePicker') && this.$('[name="DateStart"]').data('DateTimePicker').length){
                    this.$('[name="DateStart"]').data('DateTimePicker').destroy();
                }
            });
        },
        afterShowHideFieldsFilter: function() {
            var self = this;
            if (this.$('[name="DateStart"]:visible').length) {
                this.$('[name="DateStart"]').datetimepicker({
                    format: commonConfig.datePickerFormat
                }).on('dp.change', function(event){
                    self.doFilterAndSorting(event);
                });

            }else{
                if (this.$('[name="DateStart"]').data('DateTimePicker') && this.$('[name="DateStart"]').data('DateTimePicker').length){
                    this.$('[name="DateStart"]').data('DateTimePicker').destroy();
                }
            }
        }
    });
});
