// scripts/app/products/menu
define(function(require, exports, module) {
    'use strict';
    let LayoutManager = require('layoutmanager'),
        template = require('text!./template.html');
    let commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        className: 'submenu-content',
        tagName: 'ul',
        className: 'nav nav-tabs in',
        template: _.template(template),
        afterRender() {
            this.doActiveButton();
        },
        events: {
            // 'click a' : 'setActive'
        },
        doActiveButton() {
            var fragmentHash = commonFunction.getLastSplitHash();
            var classActive = 'bs-callout-active';
            this.$('a').removeClass(classActive);
            if (fragmentHash) {
                var buttonIsActive = fragmentHash.replace(/\?(.*)/ig, '');
                this.$('a[href$=' + buttonIsActive + ']').addClass(classActive);
            }
        },
        setActive(e){
        	let $Object = undefined

        	this.$('li').removeClass('active')
        	if(e && e.currentTarget){
        		$Object = $(e.currentTarget)
        	}else{
				const hash = window.location.hash
        		$Object = this.$(`[href="${hash}"]`)
        	}

        	if ($Object && $Object.length){
        		$Object.parent().addClass('active')
        	}
        }
    });
});
