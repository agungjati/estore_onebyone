// scripts/app/products
define(function(require, exports, module) {
    'use strict';
    const LayoutManager = require('layoutmanager');
    const template = require('text!./template.html');
    let commonFunction = require('commonfunction');

    module.exports = LayoutManager.extend({
        name: 'products',
        className: 'container-fluid main-content',
        template: _.template(template),
        afterRender() {
            const View = require('./menu/view')
            let view = new View()
            this.insertView('[obo-menu]', view)
            view.render()
        }
    });
});
