// scripts/app/products/product_profile
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var Model = require('./../../model');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');

    module.exports = LayoutManager.extend({
        template: _.template(template),
        initialize: function() {
            this.model = new Model();
            this.model.set(this.model.idAttribute, commonFunction.getUrlHashSplit(3));
            this.listenToOnce(this.model, 'sync', function(model) {
                this.render();
            }, this);
            this.once('afterRender', function() {
                this.model.fetch();
            });
        },
        afterRender: function(){
          eventAggregator.trigger('getProductModel', function(model){
              self.$('[obo-name="ProductName"]').text(model.toJSON().ProductName);
          });
        }
    });
});
