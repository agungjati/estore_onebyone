// scripts/app/customers/
define(function(require, exports, module) {
    'use strict';

    const
        DefaultModule = require('defaultmodule'),
        template = require('text!./template.html'),
        Table = require('./table/table'),
        Collection = require('./collection'),
        Filter = require('./filter/view'),
        Paging = require('paging'),
        ModalDialog = require('./modaldialogeditfilter/view');

    require('select2');

    module.exports = DefaultModule.extend({
        className: 'jarviswidget jarviswidget-sortable',        
        template: _.template(template),
        initialize() {
            this.table = new Table({
                collection: new Collection()
            });

            this.filter = new Filter({
                collection: this.table.collection
            });

            this.paging = new Paging({
                collection: this.table.collection
            });

            this.ModalDialog = ModalDialog

            this.on('cleanup', () => {
                this.table.remove();
            });
        },
        events: {
            'click [obo-defaultfilter]': 'setFilterToDefault',
            'click [obo-showeditfilters]': 'showEditFilters'
        },
        afterRender() {
            this.insertView('[obo-filter]', this.filter);
            this.filter.render();

            this.$('[obo-table]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.table.collection.fetch();
            this.$('select').select2();
        },
        setFilterToDefault: function() {
            this.filter.setFilterToDefault();
        },
        showEditFilters: function() {
            var self = this;
            require(['./modaldialogeditfilter/view'], function(ModalDialog) {
                self.modalDialog = new ModalDialog({
                    viewFilter: self.filter
                });

                $('body').append(self.modalDialog.el);
                self.modalDialog.$el.on('hidden.bs.modal', function() {
                    self.modalDialog.remove();
                });

                self.modalDialog.once('afterRender', function() {
                    self.modalDialog.$el.modal();
                });
                self.modalDialog.render();
            });
        }
    });
});