// scripts/app/storage/warehouse/filter
define(function(require, exports, module) {
    'use strict';
    var Filter = require('filter');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');

    module.exports = Filter.extend({
        template: _.template(template),
        defaultShowFields: ['WarehouseOwnerName', 'WarehouseName', 'Address1', 'WarehouseManagerName', 'IsActive'],
        initialize: function() {
            this.fieldsShowed = this.defaultShowFields.slice();

            commonFunction.setSelect2Status(this, {
                selector: '[name="IsActive"]',
                placeholder: 'Status'
            });
            commonFunction.setSelect2CustomerAccountOwner(this, {
                selector: '[name="WarehouseOwnerName"]',
                placeholder: 'Warehouse Owner'
            });
            commonFunction.setSelect2WarehouseManager(this,{
                selector: '[name="WarehouseManagerName"]',
                placeholder: 'Warehouse Manager'
            })
        }
    });
});
