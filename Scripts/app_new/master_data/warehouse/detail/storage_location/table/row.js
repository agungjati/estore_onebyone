// scripts/app/customers/list
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./row.html');
    const commonFunction = require('commonfunction')
    
    module.exports = LayoutManager.extend({
        tagName: 'tr',
        template: _.template(template),
        events:{
        	'click': 'showDetail'
        },
        showDetail(){
            debugger
        	window.location.hash = `${commonFunction.getCurrentHash()}/detail/${this.model.get('StorageLocationID')}/product_profile`
        }
    });
});
