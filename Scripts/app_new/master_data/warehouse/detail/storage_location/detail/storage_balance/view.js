// scripts/app/customers/detail/addresses
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager')
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var Table = require('./table/table');
    var Collection = require('./collection');
    var Paging = require('paging');

    module.exports = LayoutManager.extend({
        template: _.template(template),
        initialize: function() {
            this.table = new Table({
                collection: new Collection()
            });

            // this.table.collection.url = 'Customer/' + commonFunction.getUrlHashSplit(3) + '/Address';
            this.table.collection.url = 'Storage/StockBalance/'+ commonFunction.getUrlHashSplit(4);

            this.paging = new Paging({
                collection: this.table.collection
            })

            //   commonFunction.setSelect2AddressType(this);

            this.on('cleanup', function() {
                this.table.destroy();
                this.modalDialog && this.modalDialog.remove && this.modalDialog.remove();
            }, this)
        },
        afterRender: function() {
            this.$('[obo-table]').append(this.table.el);
            this.table.render();

            this.insertView('[obo-paging]', this.paging);
            this.paging.render();

            this.table.collection.fetch();
            // this.$('[name="Status"]').select2();
        },
        showEditFilters: function() {
            var self = this;
            require(['./modaldialogeditfilter/view'], function(ModalDialog) {
                self.showModalDialog(ModalDialog);
            });
        }
    });
});
