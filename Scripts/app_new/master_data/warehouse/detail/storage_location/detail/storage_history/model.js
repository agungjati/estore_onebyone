define(function(require, exports, module) {
    'use strict';
    var Model = require('backbone.model');

    module.exports = Model.extend({
        idAttribute: 'CustomerID',
        urlRoot: 'Storage/StockHistory',
        defaults: function() {
            return {
                StockHistoryID:'',
                InsertDate: '',
                StockHistoryName:'',
                StockTransfer: {
                    Qty:''
                },
                Product: {
                    ProductName:''
                }
            }
        }
    });
});
