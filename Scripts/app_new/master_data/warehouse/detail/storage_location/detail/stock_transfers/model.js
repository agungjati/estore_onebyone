define(function(require, exports, module) {
    'use strict';

    var Model = require('backbone.model');
    var commonFunction = require('commonfunction');

    module.exports = Model.extend({
        idAttribute: 'StockTransferID',
        defaults: function() {
            return {
                StockTransferID: '',
                InsertDate: '',
                ProductName: '',
                LocationSource: {
                    LocationCode:''
                },
                LocationDestination: {
                    LocationCode:''
                },
                Product: {
                    ProductName:''
                },
                Qty:'',
                StockTransferID : ''
            }
        }
    });
});
