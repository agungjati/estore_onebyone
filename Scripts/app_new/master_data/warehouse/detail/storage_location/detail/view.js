// scripts/app/customers/detail
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var eventAggregator = require('eventaggregator');

    module.exports = LayoutManager.extend({
        name: 'storage.warehouse.detail.storage_location.detail',
        className: 'container-fluid main-content',
        template: _.template(template),
        initialize: function(){
            var self = this;
            this.once('afterRender', function(){
                eventAggregator.trigger('getStorageWarehouseStorageLocationModel', function(model){
                    self.$('[name="LocationCode"]').text(model.get('LocationCode'));
                    self.listenTo(model, 'change:LocationCode', function(){
                        self.$('[name="LocationCode"]').text(model.get('LocationCode'));
                    });

                });
            });
        },
        afterRender: function() {
            var View = require('./menu/view');
            var view = new View();

            this.insertView('[obo-menu]', view);
            view.render();

            var oboContent = this.getView('[obo-content]');
            if (oboContent) {
                this.oboContent = oboContent;
            } else {
                if (this.oboContent) {
                    this.setView('[obo-content]', this.oboContent);
                    this.oboContent.render();
                }
            }
        }
    });
});
