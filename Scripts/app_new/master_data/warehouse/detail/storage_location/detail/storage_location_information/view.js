define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');
    require('jquery.cropit');

    module.exports = LayoutManager.extend({
        className: 'container-fluid',
        template: _.template(template),
        initialize: function() {
            var self = this;

            this.once('afterRender', function() {
                eventAggregator.trigger('getStorageWarehouseStorageLocationModel', function() {
                    self.render();
                });
            });

            this.on('cleanup', function() {
                this.modalDialog && this.modalDialog.remove && this.modalDialog.remove();
            }, this);
        },
        events: {
            'click [showdetailsketch]': 'showDetailSketch'
        },
        showDetailSketch: function() {
            var self = this;
            require(['./modaldialogshowlocationsketch/view'], function(ModalDialog) {
                self.modalDialog = new ModalDialog({
                    model: self.model
                });

                $('body').append(self.modalDialog.el);
                self.modalDialog.$el.on('hidden.bs.modal', function() {
                    self.modalDialog.remove();
                });

                self.modalDialog.once('afterRender', function() {
                    self.modalDialog.$el.modal();
                });
                self.modalDialog.render();
            });
        }
    });
});
