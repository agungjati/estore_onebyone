// scripts/app/storage/warehouse/detail/storage_location/detail/storage_location_information/edit
define(function(require, exports, module) {
    'use strict';
    var LayoutManager = require('layoutmanager');
    var template = require('text!./template.html');
    var commonFunction = require('commonfunction');
    var commonConfig = require('commonconfig');
    var Model = require('./model');
    var ladda = require('ladda.jquery');
    var eventAggregator = require('eventaggregator');
    require('bootstrap-validator');

    module.exports = LayoutManager.extend({
        className: 'container-fluid main-content',
        template: _.template(template),
        initialize: function(options) {
            var self = this;
            this.ladda = {};
            this.model = new Model();

            this.listenTo(this.model, 'request', function() {
                self.$('[obo-dosave]').attr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('start');
            });

            this.listenTo(this.model, 'sync error', function() {
                self.$('[obo-dosave]').removeAttr('disabled', 'disabled');
                self.ladda['obo-dosave'].ladda('stop');
            });

            this.listenTo(this.model, 'sync', function(model) {
                eventAggregator.trigger('getStorageWarehouseStorageLocationModel', function(model) {
                    model.set(self.model.toJSON());
                });
                Backbone.history.navigate(commonFunction.getCurrentHashOmitSuffix('1'), true);
            });

            this.once('afterRender', function() {
                eventAggregator.trigger('getStorageWarehouseStorageLocationModel', function(model) {
                    var data = model.toJSON();
                    self.model.set(data);
                    self.render();

                    commonFunction.setSelect2ZoneType(self).then(function(object) {
                        self.$(object.options.selector).val(data.ZoneType.ZoneTypeID).trigger('change');
                    });

                    commonFunction.setSelect2ZoneArea(self).then(function(object) {
                        self.$(object.options.selector).val(data.ZoneArea.ZoneAreaID).trigger('change');
                    });

                    commonFunction.setSelect2ProductSKUType(self).then(function(object) {
                        self.$(object.options.selector).val(data.SKUType.SKUTypeID).trigger('change');
                    });

                    commonFunction.setSelect2StatusStorageLocation(self,{
                        selector: '[name="IsActive"]'
                    }).then(function(object){
                        self.$(object.options.selector).val(data.IsActive.toString()).trigger('change');
                    });


                });
            });
        },
        events:{
            'change [name="Building"]': 'updateLocationCode',
            'change [name="Isle"]': 'updateLocationCode',
            'change [name="Bay"]': 'updateLocationCode',
            'change [name="Tier"]': 'updateLocationCode',
            'change [name="Files"]': 'changeTextFileField'
        },
        afterRender: function() {
            if (this.ladda['obo-dosave']) {
            }
            this.ladda['obo-dosave'] = this.$('[obo-dosave]').ladda();

            var data = this.model.toJSON();
            if (data.LocationScetchOriginalFileName){
                this.changeFileName(data.LocationScetchOriginalFileName);
            }

            this.renderValidation();
        },
        changeTextFileField: function(e){
            this.changeFileName($(e.currentTarget)[0].files[0].name);
        },
        renderValidation: function() {
            var self = this;
            this.$('[obo-form]').bootstrapValidator({
                    fields: {
                        MaxLength: {
                            validators: {
                                notEmpty: {},
                                numeric: {
                                    message: 'The Maximum Length must be a numeric number'
                                }
                            }
                        },
                        MaxWidth: {
                            validators: {
                                notEmpty: {},
                                numeric: {
                                    message: 'The Maximum Width must be a numeric number'
                                }
                            }
                        },
                        MaxHeight: {
                            validators: {
                                notEmpty: {},
                                numeric: {
                                    message: 'The Maximum Height must be a numeric number'
                                }
                            }
                        },
                        MaxWeight: {
                            validators: {
                                notEmpty: {},
                                numeric: {
                                    message: 'The Maximum Weight must be a numeric number'
                                }
                            }
                        },
                        MaxQuantity: {
                            validators: {
                                notEmpty: {},
                                numeric: {
                                    message: 'The Maximum Quantity mus be a numeric number'
                                }
                            }
                        }
                    }
                })
                .on('success.form.bv', function(e) {
                    e.preventDefault();
                    self.doSave();
                });
        },
        updateLocationCode: function() {
            this.$('[name="LocationCode"]').val(this.$('[name="Building"]').val()
            + this.$('[name="Isle"]').val()
            + this.$('[name="Bay"]').val()
            + this.$('[name="Tier"]').val());
        },
        doSave: function() {
            var data = commonFunction.formDataToJson(this.$('form').serializeArray());

            if (this.$('[name="Files"]').length != 0 && this.$('[name="Files"]')[0].files[0]) {
                data.Files = this.$('[name="Files"]')[0].files[0];
            }

            data.LocationCode = this.$('[name="LocationCode"]').val();
            this.model.save(data);
        },
        changeFileName: function(fileName){
            var selector = '[name="Files"] + .file-custom';
            this.$(selector).addClass('emptyafter');
            this.$(selector).text(fileName);
        }
    });
});
