//scripts/app/storage/warehouse/detail
define(function(require, exports, module) {
    'use strict';

    var Backbone = require('backbone');
    var Model = require('./model');
    var commonFunction = require('commonfunction');
    var eventAggregator = require('eventaggregator');

    require('backbone.subroute');

    var fnSetContentView = function(pathViewFile, replaceMainContent, options) {
        var hashtag = '#storage/warehouse';

        eventAggregator.trigger('getStorageWarehouseModel');
        require(['./' + pathViewFile + '/view'], function(View) {
            var view = new View({
                model : options && options.model
            });

            if (replaceMainContent) {
                commonFunction.setContentViewWithNewModuleView(view, hashtag);
            } else {
                var previousContentView = commonFunction.getContentView();
                var currentContentView = previousContentView && previousContentView.getView('');
                var name = previousContentView && previousContentView.getView('') && previousContentView.getView('').name;

                if (name != 'storage.warehouse.detail') {
                    var MainView = require('./view');
                    var mainView = new MainView();
                    mainView.setView('[obo-content]', view);
                    commonFunction.setContentViewWithNewModuleView(mainView, hashtag);
                } else {
                    if (currentContentView){
                        currentContentView.getView('[obo-menu]').doActiveButton();
                        currentContentView.setView('[obo-content]', view);
                        view.render();
                    }
                }
            }
        });
    };

    module.exports = Backbone.SubRoute.extend({
        initialize: function() {
            this.app = {};
            this.model = new Model();
            this.stopListening(eventAggregator, 'getStorageWarehouseModel');
            this.listenTo(eventAggregator, 'getStorageWarehouseModel', function(callback){
                var id = commonFunction.getUrlHashSplit(3);
                if (this.model.id != id){
                    this.model.clear({silent: true}).set(this.model.defaults());
                    this.model.set(this.model.idAttribute, id);
                    this.model.once('sync', function(model){
                        callback && callback(model);
                    });
                    if (!this.model.requestToServer)
                        this.model.fetch();
                }else if (!this.model.requestToServer){
                    callback && callback(this.model);
                }else{
                    this.model.once('sync', function(model){
                        callback && callback(model);
                    })
                }
            });
        },
        routes: {
            '': 'redirectToShowWarehouseProfile',
            'warehouse_profile': 'showWarehouseProfile',
            'warehouse_profile/edit': 'showWarehouseProfileEdit',
            'storage_location': 'showStorageLocation',
            'storage_location/add': 'showStorageLocationAdd',
            'storage_location/detail/:id/*subrouter': 'redirectToStorageLocationDetailModule',
            'storage_location/*subrouter': 'redirectToShowStorageLocation',
            //'*subrouter': 'redirectToShowWarehouseProfile'
        },
        showWarehouseProfile: function() {
            fnSetContentView('warehouse_profile', '', {
                model: this.model
            });
        },
        showWarehouseProfileEdit : function(){
            fnSetContentView('warehouse_profile/edit', 'replace');
        },
        showStorageLocation: function() {
            fnSetContentView('storage_location');
        },
        showStorageLocationAdd: function(){
            fnSetContentView('storage_location/add', 'replace');
        },
        redirectToStorageLocationDetailModule: function(){
            debugger;
            var self = this;
            require(['./storage_location/detail/router'], function(Router) {
                if (!self.app.storageLocationRouter) {
                    self.app.storageLocationRouter = new Router(self.prefix + '/storage_location/:id', {
                        createTrailingSlashRoutes: true
                    });
                }
            });
        },
        redirectToShowStorageLocation: function(){
            debugger
            this.navigateBackbonePrototype(commonFunction.getCurrentHashToLevel(4).replace('#', '') + '/storage_location', {
                trigger: true,
                replace: true
            });
        },
        redirectToShowWarehouseProfile: function(){
            this.navigateBackbonePrototype(commonFunction.getCurrentHashToLevel(4).replace('#', '') + '/warehouse_profile', {
                trigger: true,
                replace: true
            });
        }
    });
});
