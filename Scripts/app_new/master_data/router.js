//scripts/app/customers
define(function(require, exports, module) {
    'use strict';

    const Backbone = require('backbone');
    require('backbone.subroute');

    var fnSetContentView = pathViewFile => {
        require([pathViewFile + '/view'], View => {
            if (View)
                commonFunction.setContentViewWithNewModuleView(new View());
        });
    };

    module.exports = Backbone.SubRoute.extend({
        initialize() {
            this.app = {};
        },
        routes: {
            'customers(/*subrouter)': 'showCustomersSubRouter',
            'products(/*subrouter)': 'showProductsSubRouter',
            'warehouse(/*subrouter)': 'showWarehouseSubRouter'
        },
        showCustomersSubRouter() {
            if (!this.app.customersRouter) {
                require(['./customers/router'], Router => {
                    this.app.customersRouter = new Router(`${this.prefix}/customers`, {
                        createTrailingSlashRoutes: true
                    })
                })
            }
        },
        showProductsSubRouter() {
            if (!this.app.productsRouter) {
                require(['./products/router'], Router => {
                    this.app.productsRouter = new Router(`${this.prefix}/products`, {
                        createTrailingSlashRoutes: true
                    })
                })
            }
        },
        showWarehouseSubRouter(){
            if (!this.app.warehouseRouter) {
                require(['./warehouse/router'], Router => {
                    this.app.warehouseRouter = new Router(`${this.prefix}/warehouse`, {
                        createTrailingSlashRoutes: true
                    })
                })
            }
        }
    });
});