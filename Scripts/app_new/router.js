//scripts/app
define(function(require, exports, module) {
    'use strict';

    require('backbone');
    const commonFunction = require('commonfunction');

    const fnSetContentView = function(pathViewFile, hashtag, options) {
        if(hashtag === undefined){
            hashtag = window.location.hash
        }
        require(['./' + pathViewFile + '/view'], function(View) {
            if (View)
                commonFunction.setContentViewWithNewModuleView(new View(), hashtag, options);
        });
    };

    module.exports = Backbone.Router.extend({
        initialize: function() {
            this.app = {};
        },
        routes: {
            '': 'redirectToDashboard',
            'login': 'showLogin',
            'dashboard': 'showDashboard',
            'master_data(/*subrouter)': 'showMaster_DataSubRouter',
            'storage(/*subrouter)': 'showStorageSubRouter',
            '*actions': 'notFound'
        },
        start: function() {
            Backbone.history.start();
        },
        redirectToDashboard: function(){
            this.navigate('dashboard',{trigger:true, replace:true})
        },
        showLogin() {
            alert('anda belum login');
        },
        showDashboard: function() {
            fnSetContentView('dashboard');
        },
        showMaster_DataSubRouter: function(){
            if(!this.app.master_data){
                require(['./master_data/router'], Router => {
                    this.app.master_data = new Router('master_data', {
                        createTrailingSlashRoutes: true
                    })
                })
            }
        },
        showStorageSubRouter: function() {
            if(!this.app.storage){
                require(['./storage/router'], Router => {
                    this.app.storage = new Router('storage', {
                        createTrailingSlashRoutes: true
                    })
                })
            }
        },
        notFound: function() {
            require(['./errorpages/notfound/view'], function(View) {
                commonFunction.setContentViewWithNewModuleView(new View(), '#');
            });
        },
    });
});
